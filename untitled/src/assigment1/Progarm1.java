package assigment1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Progarm1 {
    public static void main(String[] args) {
        System.out.println("Khởi tạo dối tượng cho một Account");
 // -- khởi tạo đối tượng cho department --//
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.DepartmentName = "Sale";


        Department department2 = new Department();
        department2.departmentId = 2;
        department2.DepartmentName = "Marketing";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.DepartmentName = "Phòng ban 3";

// -- khởi tạo đối tượng cho department --//
        Position position1 = new Position();
        position1.positionID=1;
        position1.positionName=PositionName.DEV;

        Position position2 = new Position();
        position2.positionID=2;
        position2.positionName=PositionName.PM;

        Position position3 = new Position();
        position3.positionID=3;
        position3.positionName=PositionName.SCRUMMASTER;

        Position position4 = new Position();
        position4.positionID=4;
        position4.positionName=PositionName.TEST;

 // -- khởi tạo đối tượng cho department --//
        Account account1= new Account();
        account1.accountId=1;
        account1.email="ndao2k@gmail.com";
        account1.username="namdao1006";
        account1.fullName="Đào Xuân Nam";
        account1.department=department1;
        account1.position=position1;
        account1.createDate= new Date();

        Account account2= new Account();
        account2.accountId=2;
        account2.email="tienan2k@gmail.com";
        account2.username="tienan17";
        account2.fullName="Nguyễn Tiến An";
        account2.department=department2;
        account2.position=position2;
        account2.createDate= new Date();

        Account account3= new Account();
        account3.accountId=3;
        account3.email="ducthang99@gmail.com";
        account3.username="ducthang09";
        account3.fullName="Lưu Đức Thắng";
        account3.department=department1;
        account3.position=position3;
        account3.createDate= new Date();


 // Khởi tạo đối tượng Group //
        Group group1 = new Group();
        group1.groupId=1;
        group1.groupName="VTI Sale 01";
        group1.creator = account1;
        group1.createDate=new Date();

        Group group2 = new Group();
        group2.groupId=2;
        group2.groupName="VTI Sale 02";
        group2.creator = account2;
        group2.createDate=new Date();

        Group group3 = new Group();
        group3.groupId=3;
        group3.groupName="VTI Sale 03";
        group3.creator = account1;
        group3.createDate=new Date();

        Group[] groupAcc1 = { group1, group2 };
        account1.groups = groupAcc1;
        account2.groups = new Group[]{group1, group2};
        group1.accounts= new Account[]{account1};
        group2.accounts= new Account[]{account1,account2};
        group3.accounts= new Account[]{account2};


 // khởi tạo đối tượng GroupAccount //
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.groupID = group1;
        groupAccount1.accountId = account1;
        groupAccount1.joinDate= new Date();

        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.groupID = group1;
        groupAccount2.accountId = account3;
        groupAccount2.joinDate= new Date();

        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.groupID = group2;
        groupAccount3.accountId = account2;
        groupAccount3.joinDate= new Date();


 // khởi tạo đối tượng TypeQuestion//
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;
        typeQuestion1.typeName= typeName.ESSAY;

        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;
        typeQuestion2.typeName= typeName.MULTIPLECHOICE;

  // khởi tạo đối tượng CategoryQuestion//
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.CategoryId=1;
        categoryQuestion1.categoryName="Java";

        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.CategoryId=2;
        categoryQuestion2.categoryName=".NET";

        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.CategoryId=3;
        categoryQuestion3.categoryName="SQL";

  // khởi tạo đối tượng Question//
        Question question1 = new Question();
        question1.questionId=1;
        question1.content="Câu 1";
        question1.categoryId=categoryQuestion1;
      //  question1.typeId=typeQuestion1;
        question1.creatorId=account1;
        question1.createDate=new Date();

        Question question2 = new Question();
        question2.questionId=2;
        question2.content="Câu 2";
        question2.categoryId=categoryQuestion2;
      //  question2.typeId=typeQuestion2;
        question2.creatorId=account3;
        question2.createDate=new Date();

        Question question3 = new Question();
        question3.questionId=3;
        question3.content="Câu 3";
        question3.categoryId=categoryQuestion3;
     //   question3.typeId=typeQuestion2;
        question3.creatorId=account2;
        question3.createDate=new Date();

 // khởi tạo đối tượng Answer//
        Answer answer1 = new Answer();
        answer1.answerId=1;
        answer1.content="Trả lời câu 1";
        answer1.questionId=question1;
        answer1.isCorrect=true;

        Answer answer2 = new Answer();
        answer2.answerId=2;
        answer2.content="Trả lời câu 2";
        answer2.questionId=question2;
        answer2.isCorrect=false;

        Answer answer3 = new Answer();
        answer3.answerId=3;
        answer3.content="Trả lời câu 3";
        answer3.questionId=question3;
        answer3.isCorrect=true;

 // khởi tạo đối tượng Exam //
        Exam exam1= new Exam();
        exam1.examId=1;
        exam1.code="A9382";
        exam1.title="Đề thi C#";
        exam1.categoryId=categoryQuestion1;
        exam1.duration= 90;
        exam1.creatorId=account1;
        exam1.createDate = new Date();

        Exam exam2= new Exam();
        exam2.examId=2;
        exam2.code="A9102";
        exam2.title="Đề thi Java";
        exam2.categoryId=categoryQuestion2;
        exam2.duration= 120;
        exam2.creatorId=account2;
        exam2.createDate = new Date();

        Exam exam3= new Exam();
        exam3.examId=2;
        exam3.code="A9382";
        exam3.title="Đề thi SQL";
        exam3.categoryId=categoryQuestion1;
        exam3.duration=60;
        exam3.creatorId=account3;
        exam3.createDate = new Date();

 // khởi tạo đối tượng ExamQuestion//
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.examId=exam1;
        examQuestion1.questionId=question1;

        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.examId=exam2;
        examQuestion2.questionId=question2;

        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.examId=exam2;
        examQuestion3.questionId=question3;

        Exercise2();
        Exercise3();
        /*
  // Assignment2//
        System.out.println("---Question1");
        if(account2.department == null){
            System.out.println("Nhân viên này chưa có phòng ban");
        }
        else {System.out.println("Phòng ban của nhân viên này là ..." +account2.department.DepartmentName);}

 // Assignment2 :2//
        System.out.println("--Question2--");
        if(account2.groups == null){
            System.out.println("Nhân viên này chưa có group");
        }
            else {
                int dem=account2.groups.length;
                if(dem==1){
                    System.out.println("Group của nhân viên này là Java  Fresher, C# Fresher");
                }
            if(dem==2){
                System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
            }
            if(dem==3){
                System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
            }
        }

        // Assignment2 :3//
        System.out.println("--Question3--");
        System.out.println(account2.department == null ?"Nhân viên này chưa có phòng ban" : "phòng ban của " +
                "nhân viên này là"+account2.department.DepartmentName);

        // Assignment2 :4//
        System.out.println("--Question4--");
        System.out.println(account1.position.positionName.toString() == "Dev"?
                "Đây là Developer" : "Người này không phải Developer");


        // Assignment2 :5//
        System.out.println("--Question5--");
        if (group1.accounts == null ){
            System.out.println("Chưa có thành viên nào tham gia");
        }
            int sumgr = group1.accounts.length;
            switch (sumgr){
                case 1:
                    System.out.println("Nhóm có 1 thành viên");
                    break;
                case 2:
                    System.out.println("Nhóm có 2 thành viên");
                    break;
                case 3:
                    System.out.println("Nhóm có 3 thành viên");
                    break;
                default:
                    System.out.println("Nhóm có nhiều thành viên");
                    break;
            }

        // Assignment2 :6//
        System.out.println("--Question6--");
        if (account2.groups == null ){
            System.out.println("Nhân viên này chưa có groups");
        }
        int sumacc2 = account2.groups.length;
        switch (sumacc2){
            case 1:
                System.out.println("Group của nhân viên này là Java  Fresher, C# Fresher");
                break;
            case 2:
                System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
                break;
            default:
                System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
                break;
        }

        // Assignment2 :7//
        System.out.println("--Question7--");
        String postionacc1 = account1.position.positionName.toString();
        switch (postionacc1){
            case "Dev":
                System.out.println("Đây là Developer");
                break;
            default:
                System.out.println("Người này không phải là Developer");
        }

        // Assignment2 :8//
        System.out.println("--Question8--");
        Account[] arraccount = {account1,account2,account3};
        for (Account account : arraccount){
            System.out.println("AccountId: "+account.accountId);
            System.out.println("Email:"+account.email);
            System.out.println("Name: "+account.fullName);
            System.out.println("Phòng ban: "+account.department.DepartmentName);
        }

        // Assignment2 :9//
        System.out.println("--Question9--");
        Department[] departments1 = {department1,department2,department3};
        for (Department department : departments1){
            System.out.println("Thông tin Department thứ " +department.departmentId+"là:");
            System.out.println("id"+department.departmentId);
            System.out.println("Name:"+department.DepartmentName);
        }

        // Assignment2 :10//
        System.out.println("--Question10--");
        for (int i=1;i<arraccount.length;i++){
            System.out.println("AccountId: "+(i+1));
            System.out.println("Email:"+arraccount[i].email);
            System.out.println("Name: "+arraccount[i].fullName);
            System.out.println("Phòng ban: "+arraccount[i].department.DepartmentName);
        }

        // Assignment2 :11//
        System.out.println("--Question11--");
        for (int a=0;a<departments1.length;a++) {
            System.out.println("Thông tin Department thứ " + (a + 1) + "là:");
            System.out.println("id" + departments1[a].departmentId);
            System.out.println("Name:" + departments1[a].DepartmentName);
        }

        // Assignment2 :12//
        System.out.println("--Question12--");
        for (int b=0;b<3;b++){
            System.out.println("AccountId: "+(b+1));
            System.out.println("Email:"+arraccount[b].email);
            System.out.println("Name: "+arraccount[b].fullName);
            System.out.println("Phòng ban: "+arraccount[b].department.DepartmentName);
        }

        // Assignment2 :13//
        System.out.println("--Question13--");
        for (int c=0;c<arraccount.length;c++) {
            if (c !=1) {
                System.out.println("AccountId: " + (c + 1));
                System.out.println("Email:" + arraccount[c].email);
                System.out.println("Name: " + arraccount[c].fullName);
                System.out.println("Phòng ban: " + arraccount[c].department.DepartmentName);
            }
        }

        // Assignment2 :14//
        System.out.println("--Question14--");
        for (int d=0;d<arraccount.length; d++) {
            if (arraccount[d].accountId < 4) {
                System.out.println("AccountId: " + (d + 1));
                System.out.println("Email:" + arraccount[d].email);
                System.out.println("Name: " + arraccount[d].fullName);
                System.out.println("Phòng ban: " + arraccount[d].department.DepartmentName);
            }
        }

        // Assignment2 :15//
        System.out.println("--Question15--");
        for (int e=0;e<=20;e++){
            if(e%2==0){
                System.out.println(e+"");
            }
        }

        // Assignment2 :16//
        System.out.println("--Question16--");


         */
    }


public static void Exercise2(){
        // Question1//
    System.out.println("--Question 1--");
    int i=5;
    System.out.println("Sỗ nguyên vừa in ra là :"+i);

    // Question2//
    System.out.println("--Question 2--");
    int j=100000000;
    System.out.println(j);

    // Question3//
    System.out.println("--Question 3--");
    float a=5.567098f;
    System.out.println(a);

    // Question4//
    System.out.println("--Question4--");
    String s= "Đào Xuân Nam";
    System.out.println("Tên tôi là "+s+"và tôi đang độc thân");

    // Question5//
    System.out.println("--Question5--");
    String s1 = "dd/MM/yyyy HH:mm:ss";
    SimpleDateFormat sf = new SimpleDateFormat(s1);
    String date = sf.format(new Date());
    System.out.println(date);

    // Question6//
    System.out.println("--Question6--");

}
    public static void Exercise3() {
        Exam exam4 = new Exam();
        exam4.examId = 4;
        exam4.createDate = new Date();

        // Question1//
        System.out.println("--Question1--");
        Locale locale = new Locale("vn", "VN");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String date = dateFormat.format(exam4.createDate);
        System.out.println(exam4.examId + ":" + date);

        // Question2//
        System.out.println("--Question2--");
        String pattern = "yyyy-MM-dd-HH-mm-ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Exam[] exams = {exam4};
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.examId + ": " + date);
        }

        // Question3//
        System.out.println("--Question3--");
        String s = "yyyy";
        simpleDateFormat = new SimpleDateFormat(s);
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.examId + ": " + date);
        }

        // Question4//
        System.out.println("--Question4--");
        String s1 = "yyyy-MM";
        simpleDateFormat = new SimpleDateFormat(s1);
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.examId + ": " + date);
        }

        // Question5//
        System.out.println("--Question5--");
        String s2 = "MM-dd";
        simpleDateFormat = new SimpleDateFormat(s2);
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.examId + ": " + date);
        }
    }
    public static void Exercise4(){
        // Question1//
        System.out.println("--Question1--");
        Random random = new Random();
        int number = random.nextInt();
        System.out.println("Số nguyên bất kỳ là :"+number);

        // Question2//
        System.out.println("--Question2--");
        float number1 = random.nextFloat();
        System.out.println("Số nguyên bất kỳ là :"+number1);

        // Question3//
        System.out.println("--Question3--");
        String[] arrName ={"Đào Xuân Nam","Lưu Đức Thắng","Nguyễn Tiến An"};
        int i=random.nextInt(arrName.length);
        System.out.println("Tên ngẫu nhiên của bạn trong lớp là :"+arrName[i]);

        // Question4//
        System.out.println("--Question4--");
        int minDate = (int) LocalDate.of(1995,07,24).toEpochDay();
        int maxDate = (int) LocalDate.of(1995,12,20).toEpochDay();
        long randomlong = minDate + random.nextInt(maxDate-minDate);
        LocalDate randomday = LocalDate.ofEpochDay(randomlong);
        System.out.println(randomday);

        // Question5//
        System.out.println("--Question5--");
        int minDate1 = (int) LocalDate.now().toEpochDay();
        long random1 = minDate1-random.nextInt(365);
        LocalDate randomlong1 = LocalDate.ofEpochDay(random1);
        System.out.println(randomlong1);

        // Question6//
        System.out.println("--Question6--");
        long random2 = random.nextInt();
        LocalDate randomlong2 = LocalDate.ofEpochDay(random2);
        System.out.println(randomlong2);

        // Question7//
        System.out.println("--Question7--");
        int j = random.nextInt(899)+100;
        System.out.println(j);
    }
    public static  void Exercise5(){
        // Question1//
        System.out.println("--Question1--");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời bạn nhập vào số nguyên thứ nhất :");
        int number = scanner.nextInt();
        System.out.println("Mời bạn nhập vào số nguyên thứ hai :");
        int number1 = scanner.nextInt();
        System.out.println("Mời bạn nhập vào số nguyên thứ ba :");
        int number2 = scanner.nextInt();
        System.out.println("Các số bạn vừa nhập là :"+number+" "+number1+" "+number2);

        // Question2//
        System.out.println("--Question2--");
        System.out.println("Mời bạn nhập vào số thực thứ nhất :");
        float number3 = scanner.nextFloat();
        System.out.println("Mời bạn nhập vào số thực thứ hai :");
        float number4 = scanner.nextFloat();
        System.out.println("Các số bạn vừa nhập là :"+number3+" "+number4);

        // Question3//
        System.out.println("--Question3--");
        System.out.println("Mời bạn nhập họ :");
        String s = scanner.nextLine();
        System.out.println("Mời bạn nhập tên :");
        String s1 = scanner.nextLine();
        System.out.println("Họ và tên của bạn là:"+s+" "+s1);

        // Question4//
        System.out.println("--Question4--");
        System.out.println("Mời bạn nhập năm :");
        int year = scanner.nextInt();
        System.out.println("Mời bạn nhâp tháng :");
        int month = scanner.nextInt();
        System.out.println("Mời bạn nhập ngày :");
        int day = scanner.nextInt();
        LocalDate date = LocalDate.of(year,month,day);
        System.out.println("Ngày sinh của bạn là :"+date);

        // Question5//
        System.out.println("--Question5--");
        Account account=new Account();
        System.out.println("Nhập ID :");
        account.accountId = scanner.nextInt();
        System.out.println("Nhập Email :");
        account.email=scanner.nextLine();
        System.out.println("Nhập UserName :");
        account.username=scanner.nextLine();
        System.out.println("Nhập FullName :");
        account.fullName=scanner.nextLine();
        System.out.println("Nhập Position");
        int pos = scanner.nextInt();
        switch (pos){
            case 1 :
                Position pos1= new Position();
                pos1.positionName=PositionName.DEV;
                account.position=pos1;
                break;
            case 2 :
                Position pos2= new Position();
                pos2.positionName=PositionName.TEST;
                account.position=pos2;
                break;
            case 3 :
                Position pos3= new Position();
                pos3.positionName=PositionName.SCRUMMASTER;
                account.position=pos3;
                break;
            case 4 :
                Position pos4= new Position();
                pos4.positionName=PositionName.PM;
                account.position=pos4;
                break;
        }
        System.out.println("Thông tin account vừa nhập là :" +
                "accountid :"+account.accountId+
                "email :"+account.email+
                "username:"+account.username+
                "fullname:"+account.fullName+
                "position:"+account.position);

        // Question6//
        System.out.println("--Question6--");
        Department department = new Department();
        System.out.println("Mời bạn nhập departmentID:");
        department.departmentId=scanner.nextInt();
        System.out.println("Mời ban nhập departmentName:");
        department.DepartmentName=scanner.nextLine();
        System.out.println("Thông tin department vừa nhập là :" +
                "departmentID :"+department.departmentId+
                "departmentName: "+department.DepartmentName);

        // Question7//
        System.out.println("--Question7--");
        int a;
        System.out.println("Mời bạn nhập số");
        while (true){
            try {
                a=Integer.parseInt(scanner.nextLine());
                a=scanner.nextInt();
                if (a%2==0){
                System.out.println("Bạn nhập đúng");
            }break;
            }catch (Exception exception){
                System.out.println("Bạn nhập sai, mời nhập lại");
            }
        }

        // Question8//
        System.out.println("--Question8--");
        System.out.println("Mời bạn nhập chức năng muốn sử dụng:" +
                "Nhập 1: tạo account" +
                "Nhập 2: tạo department");
        int b;
        while (true){
            b=scanner.nextInt();
            if(b==1){
                Account account1=new Account();
                System.out.println("Nhập ID :");
                account1.accountId = scanner.nextInt();
                System.out.println("Nhập Email :");
                account1.email=scanner.nextLine();
                System.out.println("Nhập UserName :");
                account1.username=scanner.nextLine();
                System.out.println("Nhập FullName :");
                account1.fullName=scanner.nextLine();
                System.out.println("Nhập Position");
                System.out.println("Thông tin account vừa nhập là :" +
                        "accountid :"+account1.accountId+
                        "email :"+account1.email+
                        "username:"+account1.username+
                        "fullname:"+account1.fullName);
            }else if(b==2){
                Department department1 = new Department();
                System.out.println("Mời bạn nhập departmentID:");
                department1.departmentId=scanner.nextInt();
                System.out.println("Mời ban nhập departmentName:");
                department1.DepartmentName=scanner.nextLine();
                System.out.println("Thông tin department vừa nhập là :" +
                        "departmentID :"+department1.departmentId+
                        "departmentName: "+department1.DepartmentName);
            }else {
                System.out.println("Nhập sai, Nhập lại:");
            }
        }
    }

}
