package com.vti.frontend;

import com.vti.entity.Student;

public class Program2 {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(student); //grading = 0-> yếu
        System.out.println("----------");
        student.setGrading(5);//grading =5 -> trung bình
        System.out.println(student);
        System.out.println("----------");
        student.addGrading(2);//grading =7 ->khá
        System.out.println(student);
    }
}
