package com.vti.frontend;
import com.vti.backend.*;
import com.vti.entity.CanBo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // danh sách
        List<CanBo> canBoList = new ArrayList<>();
        QLCB qlcb = new QLCB();


        while (true){
            System.out.println("Mời bạn chọn chức năng: ");
            System.out.println("1: Thêm mới cán bộ");
            System.out.println("2: Tìm kiếm theo họ tên");
            System.out.println("3: Hiển thị thông tin danh sách cán bộ");
            System.out.println("4: Nhấp vào tên của cán bộ và delete");
            System.out.println("5: Thoát chương trình");
            int chose  = Integer.parseInt(scanner.nextLine());

        switch (chose){
            case 1:
                CanBo canBo1 = qlcb.addCanbo();
                canBoList.add(canBo1);
                break;
            case 2:
                CanBo canBo2= qlcb.timkiem(canBoList);
                System.out.println(canBo2);
                break;
            case 3:
                for (CanBo cb: canBoList){
                    System.out.println(cb);
                }
                break;
            case 4:
                CanBo canBo3 = qlcb.timkiem(canBoList);
                canBoList.remove(canBo3);
                System.out.println("Đã xóa thành công: ");
                break;
            case 5:
                return;
        }
    }
    }
}
