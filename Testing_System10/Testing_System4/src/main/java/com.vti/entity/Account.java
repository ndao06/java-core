package com.vti.entity;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class Account {
    private int accountId;
    public String email;
    private String username;
    private String fullName;
    public Department department;
    public Position position;
    public LocalDate createDate;
    public Group[] groups;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public Account(int accountId, String email, String username, String fullName, Position position, LocalDate createDate) {
        this.accountId = accountId;
        this.email = email;
        this.username = username;
        this.fullName = fullName;
        this.position = position;
        this.createDate = createDate;
    }

    public Account(int accountId, String email, String username, String fullName, Position position) {
        this.accountId = accountId;
        this.email = email;
        this.username = username;
        this.fullName = fullName;
        this.position = position;
    }

    public Account(int accountId, String email, String username, String fullName) {
        this.accountId = accountId;
        this.email = email;
        this.username = username;
        this.fullName = fullName;
        Account account = new Account(2,"ndao2k","namdao1006","Đào Xuân Nam");
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }


    public void setCreateDate(Date createDate) {
        this.createDate = LocalDate.now();
    }

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(Group[] groups) {
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public Account() {
    }

    public Account(int accountId, String email, String username, String fullName, Department department, Position position, Date createDate, Group[] groups) {
        this.accountId = accountId;
        this.email = email;
        this.username = username;
        this.fullName = fullName;
        this.department = department;
        this.position = position;
        this.createDate = LocalDate.now();
        this.groups = groups;
    }
}
