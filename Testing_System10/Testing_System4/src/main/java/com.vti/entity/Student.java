package com.vti.entity;

import java.util.Scanner;

public class Student {
    private int id;
    private String name;
    private String hometown;
    private float grading;

    @Override
    public String toString() {
        String gradingType = "";
        if(this.grading<4){
            gradingType ="Yếu";
        }else if(this.grading>=4 && this.grading<6){
            gradingType="Trung bình";
        }else if(this.grading>=6 && this.grading<6){
            gradingType="Khá";
        }else {
            gradingType="giỏi";
        }
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hometown='" + hometown + '\'' +
                ", grading=" + gradingType +
                '}';
    }

    public void setGrading(float grading) {
        this.grading = grading;
    }

    public void addGrading(float grading){
        this.grading +=grading;
    }



    public Student(){
        Scanner scanner= new Scanner(System.in);
        System.out.println("Nhập vào tên: ");
        String name = scanner.nextLine();
        this.name=name;

        System.out.println("Nhập vào địa chỉ: ");
        String hometown = scanner.nextLine();
        this.hometown = hometown;

        this.grading=0;
    }
}
