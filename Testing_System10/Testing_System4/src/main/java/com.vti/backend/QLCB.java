package com.vti.backend;

import com.vti.entity.CanBo;
import com.vti.entity.Gender;
import com.vti.utilts.ScannerUtils;

import java.util.List;
import java.util.Scanner;

public class QLCB {
    public CanBo addCanbo(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời nhập vào tên cán bộ: ");
        String fullName = ScannerUtils.inputString();
        System.out.println("Mời nhập vào tuổi: ");
        int age = Integer.parseInt(scanner.nextLine());
        System.out.println("Chọn giới tính của cán bộ: ");

        Gender gender;
        System.out.println("1: Nam");
        System.out.println("2: Nữ");
        System.out.println("3: Khác");
        int chose = Integer.parseInt(scanner.nextLine());
        switch (chose){
            case 1:
                gender = Gender.Male;
                break;
            case 2:
                gender=Gender.Female;
                break;
            default:
                gender = Gender.khac;
        }
        System.out.println("Mời bạn nhập địa chỉ cán bộ: ");
        String address = scanner.nextLine();

        //cách 2
        CanBo canBo1 = new CanBo();
        canBo1.setFullName(fullName);
        canBo1.setAge(6);

        CanBo canBo= new CanBo(fullName,age,gender,address);
        System.out.println(canBo);

        return canBo;
    }

    public  CanBo timkiem(List<CanBo> canBoList){
        System.out.println("Nhập vào họ tên mà bạn tìm kiếm");
        Scanner scanner= new Scanner(System.in);
        String fullName= scanner.nextLine();
        CanBo canBo= new CanBo();
        for (CanBo cb: canBoList){
            if(fullName.equals(cb.getFullName())){
                canBo= cb;
                break;
            }
        }
        return canBo;
    }


}
