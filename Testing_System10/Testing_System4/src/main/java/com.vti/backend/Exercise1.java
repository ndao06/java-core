package com.vti.backend;
import com.vti.entity.*;

import java.time.LocalDate;

public class Exercise1 {
    public static void main(String[] args) {
        Exercise1 exercise1= new Exercise1();


        System.out.println();
    }

    public void question1(){
        Department department = new Department();
        Department department1 = new Department("dep1");
    }



    public void question2(){
        Account account = new Account();
        Account account1 = new Account(2,"ndao2k","namdao1006","Đào Xuân Nam");
        Position position= new Position();
        Account account2 = new Account(1,"thang99","thang","Lưu Đức Thắng",position);
        System.out.println(account2.createDate);
        Position position1 = new Position();
        Account account3 = new Account(3,"an99","an","Nguyễn Tiến An",position1, LocalDate.of(2022,6,7));
        System.out.println(account3);
    }
}
