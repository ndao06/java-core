package com.vti.entity;

import java.time.LocalDate;

public class Question {
    public int id;
    public String content;
    public CategoryQuestion categoryQuestionId;
    public TypeQuestion typeQuestionId;
    public Account accountId;
    public LocalDate createDate;
}
