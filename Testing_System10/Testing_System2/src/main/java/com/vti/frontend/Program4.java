package com.vti.frontend;

import com.vti.backend.Exercise4;
import com.vti.utils.ScannerUtils;

public class Program4 {
    public static void main(String[] args) {
        System.out.println("Chọn menu");
        System.out.println("1: Xem question");
        System.out.println("2: Thoát");
        System.out.println("Mời nhập: ");
        int a= ScannerUtils.inputNumber();
            switch (a){
                case 1:
                    while (true){
                        System.out.println("-----------Exercise4----------");
                        System.out.println("1: question1");
                        System.out.println("2: question2");
                        System.out.println("3: question3");
                        System.out.println("4: question4");
                        System.out.println("5: question5");
                        System.out.println("6: question6");
                        System.out.println("7: question7");
                        System.out.println("Nhập question muốn xem: ");
                        int chose = ScannerUtils.inputNumber();

                        switch (chose){
                            case 1:
                                Exercise4.question1();
                                break;
                            case 2:
                                Exercise4.question2();
                                break;
                            case 3:
                                Exercise4.question3();
                                break;
                            case 4:
                                Exercise4.question4();
                                break;
                            case 5:
                                Exercise4.question5();
                                break;
                            case 6:
                                Exercise4.question6();
                                break;
                            case 7:
                                Exercise4.question7();
                                break;
                            default:
                                System.out.println("Chỉ nhập từ 1-7, mời bạn nhập lại: ");
                                chose = ScannerUtils.inputNumber();
                        }
                        break;
                    }
                case 2:
                    return;
                default:
                    System.out.println("Mời nhập lại:");
                    a=ScannerUtils.inputNumber();
            }
        }
    }

