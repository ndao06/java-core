package com.vti.frontend;

import com.vti.backend.Exercise6;
import com.vti.utils.ScannerUtils;

public class Program6 {
    public static void main(String[] args) {
        while (true){
            System.out.println("---------Exercise6---------:");
            System.out.println("1: question1");
            System.out.println("2: question2");
            System.out.println("3: question3");
            int chose= ScannerUtils.inputNumber();

            switch (chose){
                case 1:
                    Exercise6.question1();
                case 2:
                    Exercise6.question2();
                case 3:
                    Exercise6.question3();
                default:
                    System.out.println("Mời nhập lại: ");
                    chose= ScannerUtils.inputNumber();
            }
            return;
        }
    }
}
