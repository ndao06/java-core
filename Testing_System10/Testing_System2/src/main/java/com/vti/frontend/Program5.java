package com.vti.frontend;


import com.vti.backend.Exercise5;
import com.vti.utils.ScannerUtils;

public class Program5 {
    public static void main(String[] args) {

        while (true){
            System.out.println("----------Exercise5----------");
            System.out.println("Question1 -> question9");
            System.out.println("Chọn từ 1 -> 9 để xem các question");
            System.out.println("Mời chọn question muốn xem:");
            int chose= ScannerUtils.inputNumber();

            switch (chose){
                case 1:
                    Exercise5.question1();
                    break;
                case 2:
                    Exercise5.question2();
                    break;
                case 3:
                    Exercise5.question3();
                    break;
                case 4:
                    Exercise5.question4();
                    break;
                case 5:
                    Exercise5.question5();
                    break;
                case 6:
                    Exercise5.question6();
                    break;
                case 7:
                    Exercise5.question7();
                    break;
                case 8:
                    Exercise5.question8();
                    break;
                case 9:
                    Exercise5.question9();
                    break;
                default:
                    System.out.println("Mời nhập lại: ");
                    chose=ScannerUtils.inputNumber();
            }
            return;
        }
    }
}
