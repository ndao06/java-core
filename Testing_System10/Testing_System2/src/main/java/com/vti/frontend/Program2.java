package com.vti.frontend;

import com.vti.backend.Exercise2;
import com.vti.utils.ScannerUtils;

public class Program2 {
    public static void main(String[] args) {
        while (true) {
            System.out.println("---------Exercise2---------:");
            System.out.println("1: question1");
            System.out.println("2: question2");
            System.out.println("3: question3");
            System.out.println("4: question4");
            System.out.println("5: question5");
            System.out.println("Mời bạn nhập từ 1-5 để chọn question muốn xem: ");
            int chose = ScannerUtils.inputNumber();

            switch (chose){
                case 1:
                    Exercise2.question1();
                    break;
                case 2:
                    Exercise2.question2();
                    break;
                case 3:
                    Exercise2.question3();
                    break;
                case 4:
                    Exercise2.question4();
                    break;
                case 5:
                    Exercise2.question5();
                    break;
                default:
                    System.out.println("Mời bạn nhập lại để chọn question muốn xem: ");
                    chose = ScannerUtils.inputNumber();
            }
        }
    }
}