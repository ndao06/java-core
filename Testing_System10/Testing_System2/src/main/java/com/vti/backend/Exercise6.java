package com.vti.backend;
import com.vti.entity.*;

import java.time.LocalDate;

public class Exercise6 {
    public static void question1(){
        for (int i = 0; i < 10; i++) {
            if(i%2==0){
                System.out.println(i+" ");
            }
        }
        System.out.println("");
    }

    public static void question2(){
        Account account = new Account();
        account.id=1;
        account.email="ndao2k";
        account.userName="nam";
        account.fullName="Đào Xuân Nam";
        account.createDate= LocalDate.now();

        Account account1 = new Account();
        account1.id=2;
        account1.email="an99";
        account1.userName="an";
        account1.fullName="Nguyễn Tiến An";
        account1.createDate= LocalDate.now();

        Account account2 = new Account();
        account2.id=3;
        account2.email="thang99";
        account2.userName="thang";
        account2.fullName="Lưu Đức Thắng";
        account2.createDate= LocalDate.now();

        Account[] arraccount = {account,account1,account2};
        for (int i = 0; i < arraccount.length; i++) {
            System.out.println("ID: "+arraccount[i].id+
                    "Email: "+arraccount[i].email+
                    "UserName: "+arraccount[i].userName+
                    "FullName: "+arraccount[i].fullName+
                    "CreateDate"+arraccount[i].createDate);
        }
    }

    public static void question3(){
        for (int i = 0; i < 10; i++) {
            System.out.println(i+"");
        }
    }
}
