package com.vti.backend;
import com.vti.entity.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Locale;

public class Exercise3 {
    /*
    public static void question() {

        Exercise3 exercise3 = new Exercise3();

        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Marketing";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Sale";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "CSKH";

        //Khởi tạo Position
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.Dev;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.Test;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.ScrumMaster;

        Position position4 = new Position();
        position4.positionId = 4;
        position4.positionName = PositionName.PM;

        //Khởi tạo Group
        Group group1 = new Group();
        group1.id = 1;
        group1.name = "Testing System";

        Group group2 = new Group();
        group2.id = 2;
        group2.name = "Development";

        Group group3 = new Group();
        group3.id = 3;
        group3.name = "Sale";

        //Khởi tạo account
        Account account1 = new Account();
        account1.id = 1;
        account1.email = "ndao2k";
        account1.userName = "nam";
        account1.fullName = "Đào Xuân Nam";
        account1.department = department1;
        account1.position = position1;
        account1.createDate = LocalDate.now();
        Group[] groupAcc1 = {group1, group2};
        account1.groups = groupAcc1;

        Account account2 = new Account();
        account2.id = 2;
        account2.email = "ducthang99";
        account2.userName = "thang";
        account2.fullName = "Lưu Đức Thắng";
        account2.department = department2;
        account2.position = position2;
        account2.createDate = LocalDate.of(2021, 3, 4);
        account2.groups = new Group[]{group1, group2};

        Account account3 = new Account();
        account3.id = 3;
        account3.email = "annguyen99";
        account3.userName = "an";
        account3.fullName = "Nguyễn Tiến An";
        account3.department = department3;
        account3.position = position2;
        account3.createDate = LocalDate.now();

        group1.accounts = new Account[]{account1};
        group2.accounts = new Account[]{account1, account2};
        group3.accounts = new Account[]{account2};

        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.groupId = group1;
        groupAccount1.accountId = account1;

        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.groupId = group2;
        groupAccount2.accountId = account2;

        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.groupId = group1;
        groupAccount3.accountId = account1;

        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.id = 1;
        typeQuestion1.typeName = TypeName.Essay;

        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.id = 2;
        typeQuestion2.typeName = TypeName.MultipleChoice;

        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.id = 1;
        categoryQuestion1.categoryName = "Java";

        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.id = 2;
        categoryQuestion2.categoryName = "SQL";

        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.id = 3;
        categoryQuestion3.categoryName = ".NET";

        Question question1 = new Question();
        question1.id = 1;
        question1.content = "Câu 1";
        question1.categoryQuestionId = categoryQuestion1;
        question1.typeQuestionId = typeQuestion1;
        question1.accountId = account1;
        question1.createDate = LocalDate.now();

        Question question2 = new Question();
        question2.id = 2;
        question2.content = "Câu 2";
        question2.categoryQuestionId = categoryQuestion2;
        question2.typeQuestionId = typeQuestion2;
        question2.accountId = account2;
        question2.createDate = LocalDate.of(2022, 5, 7);

        Question question3 = new Question();
        question3.id = 1;
        question3.content = "Câu 3";
        question3.categoryQuestionId = categoryQuestion3;
        question3.typeQuestionId = typeQuestion1;
        question3.accountId = account3;
        question3.createDate = LocalDate.now();

        Answer answer1 = new Answer();
        answer1.id = 1;
        answer1.content = "Trả lời 1";
        answer1.question = question1;
        answer1.isCorrect = true;

        Answer answer2 = new Answer();
        answer2.id = 2;
        answer2.content = "Trả lời 2";
        answer2.question = question3;
        answer2.isCorrect = false;

        Answer answer3 = new Answer();
        answer3.id = 3;
        answer3.content = "Trả lời 3";
        answer3.question = question2;
        answer3.isCorrect = true;

        Exam exam1 = new Exam();
        exam1.id = 1;
        exam1.code = "A9382";
        exam1.title = "Đề thi Java";
        exam1.category = categoryQuestion1;
        exam1.duration = 90;
        exam1.creator = account1;
        exam1.createDate = LocalDate.of(2022, 6, 7);

        Exam exam2 = new Exam();
        exam2.id = 2;
        exam2.code = "A9102";
        exam2.title = "Đề thi SQL";
        exam2.category = categoryQuestion2;
        exam2.duration = 60;
        exam2.creator = account2;
        exam2.createDate = LocalDate.now();

        Exam exam3 = new Exam();
        exam3.id = 3;
        exam3.code = "A0123";
        exam3.title = "Đề thi .NET";
        exam3.category = categoryQuestion3;
        exam3.duration = 120;
        exam3.creator = account3;
        exam3.createDate = LocalDate.of(2022, 4, 7);

        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.examId = exam1;
        examQuestion1.questionId = question1;

        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.examId = exam2;
        examQuestion2.questionId = question2;

        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.examId = exam3;
        examQuestion3.questionId = question3;
    }

    public static void question1() {
        //question1
        System.out.println("------question1------");
        Locale locale = new Locale("vn", "VN");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String date = dateFormat.format(exam1.createDate);
        System.out.println(exam1.code + " " + date);
    }

    public static void question2() {
        //question2
        System.out.println("------question2------");
        String pattern = "yyyy-MM-dd-HH-mm-ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Exam[] exams = {exam1, exam2};
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.code + ": " + date);
        }
    }

    public static void question3() {
        //question3
        System.out.println("------question3------");
        String pattern1 = "yyyy";
        simpleDateFormat = new SimpleDateFormat(pattern1);
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.code + ": " + date);
        }
    }

    public static void question4() {
        //question4
        System.out.println("------question4------");
        String patter2 = "yyyy-MM";
        simpleDateFormat = new SimpleDateFormat(patter2);
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.code + ": " + date);
        }
    }

    public static void question5() {
        //question5
        System.out.println("------question5------");
        String pattern3 = "MM-dd";
        simpleDateFormat = new SimpleDateFormat(pattern3);
        for (Exam exam : exams) {
            date = simpleDateFormat.format(exam.createDate);
            System.out.println(exam.code + ": " + date);
        }
    }
    */

}
