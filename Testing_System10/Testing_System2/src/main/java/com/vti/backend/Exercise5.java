package com.vti.backend;

import com.vti.entity.Account;
import com.vti.entity.Department;
import com.vti.entity.Position;
import com.vti.entity.PositionName;
import com.vti.utils.ScannerUtils;

import java.time.LocalDate;
import java.util.Scanner;

public class Exercise5 {
    public static void question1() {
        System.out.println("-----question1");
        System.out.println("Mời nhập số thứ nhất: ");
        int a = ScannerUtils.inputNumber();
        System.out.println("Mời nhập số thứ hai: ");
        int b = ScannerUtils.inputNumber();
        System.out.println("Mời nhập số thứ ba: ");
        int c = ScannerUtils.inputNumber();
        System.out.println("Các số vừa nhập là :" + a + " " + b + " " + c);
    }

    public static void question2() {
        Scanner scanner= new Scanner(System.in);
        System.out.println("-----question2");
        System.out.println("Mời nhập số thực thứ nhất là: ");
        float a = scanner.nextFloat();
        System.out.println("Mời nhập số thực thứ hai là: ");
        float b = scanner.nextFloat();
        System.out.println("Hai số vừa nhập là: " + a + " " + b);
    }

    public static void question3() {
        System.out.println("-----question3");
        System.out.println("Mời bạn nhập họ: ");
        String s = ScannerUtils.inputString();
        System.out.println("Mời bạn nhập tên: ");
        String s1 = ScannerUtils.inputString();
        System.out.println("Họ và tên là: " + s + " " + s1);
    }

    public static void question4() {
        System.out.println("-----question4");
        System.out.println("Mời bạn nhập năm sinh: ");
        int a = ScannerUtils.inputNumber();
        System.out.println("Mời bạn nhập tháng sinh: ");
        int b = ScannerUtils.inputNumber();
        System.out.println("Mời bạn nhập ngày sinh: ");
        int c = ScannerUtils.inputNumber();
        LocalDate date = LocalDate.of(a, b, c);
        System.out.println("Ngày sinh của bạn là: " + date);
    }

    public static void question5() {
        System.out.println("-----question5");
        Account account = new Account();
        System.out.println("Nhập Id: ");
        account.id = ScannerUtils.inputNumber();
        System.out.println("Nhập Email: ");
        account.email = ScannerUtils.inputString();
        System.out.println("Nhập UserName: ");
        account.userName = ScannerUtils.inputString();
        System.out.println("Nhập FullName: ");
        account.fullName = ScannerUtils.inputString();
        System.out.println("Nhập position:" +
                "1:Dev" +
                "2:Test" +
                "3:Scrum_Master" +
                "4:PM");
        int a = ScannerUtils.inputNumber();
        switch (a) {
            case 1:
                Position position1 = new Position();
                position1.positionName = PositionName.Dev;
                account.position = position1;
            case 2:
                Position position2 = new Position();
                position2.positionName = PositionName.Test;
                account.position = position2;
            case 3:
                Position position3 = new Position();
                position3.positionName = PositionName.ScrumMaster;
                account.position = position3;
            case 4:
                Position position4 = new Position();
                position4.positionName = PositionName.PM;
                account.position = position4;
        }
        System.out.println("Thông tin account vừa nhập, ID: " + account.id + "" +
                "Email: " + account.email +
                "UserName: " + account.userName +
                "FullName: " + account.fullName +
                "Position: " + account.position.positionName);
    }

    public static void question6() {
        System.out.println("-----question6");
        System.out.println("Mời bạn nhập thông tin Department: ");
        Department department = new Department();
        System.out.println("Nhập ID: ");
        department.departmentId = ScannerUtils.inputNumber();
        System.out.println("Nhập Name: ");
        department.departmentName = ScannerUtils.inputString();

        System.out.println("Thông tin Department vừa nhập, ID: " + department.departmentId +
                "Name" + department.departmentName);
    }

    public static void question7() {
        System.out.println("-----question7");
        while (true) {
            System.out.println("Nhập số chẵn: ");
            int a = ScannerUtils.inputNumber();
            if (a % 2 == 0) {
                System.out.println("Bạn vừa nhập số: " + a);
                return;
            } else {
                System.out.println("Bạn nhập sai, đây không phải số chẵn, mời nhập lại");
            }
        }
    }

    public static void question8() {
        System.out.println("-----question8");
        int a;
        while (true) {
            System.out.println("Mời bạn chọn chức năng:" +
                    "1:Tạo Account," +
                    "2:Tạo Department");
            a = ScannerUtils.inputNumber();
            if (a == 1 || a == 2) {
                switch (a) {
                    case 1:
                        question5();
                        break;
                    case 2:
                        question7();
                        break;
                }
                return;
            } else {
                System.out.println("Mời bạn nhập lại");
            }
        }
    }

    public  static void question9() {
        System.out.println("-----question9");
        int a;
        while (true) {
            System.out.println("Mời bạn chọn chức năng:" +
                    "1:Tạo Account," +
                    "2:Tạo Department"+
                    "3:Add Group vào Account");
            a = ScannerUtils.inputNumber();
            if (a == 1 || a == 2 ||a==3) {
                switch (a) {
                    case 1:
                        question5();
                        break;
                    case 2:
                        question7();
                        break;
                    case 3:
                        question9();
                        break;
                }
                System.out.println("Bạn có muốn nhập tiếp hay không,chọn 0 để kết thúc");
                int b= ScannerUtils.inputNumber();
                if(b==0){
                    System.out.println("Kết thúc");
                    return;
                }else {
                    System.out.println("Nhập lại");
                }
            }
        }
    }
}