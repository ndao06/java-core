package com.vti.backend;

import com.vti.entity.*;

import java.time.LocalDate;

public class Exercise1 {
    /*
    public static void question() {
        //Khởi tạo departmen
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Marketing";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Sale";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "CSKH";

        //Khởi tạo Position
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.Dev;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.Test;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.ScrumMaster;

        Position position4 = new Position();
        position4.positionId = 4;
        position4.positionName = PositionName.PM;

        //Khởi tạo Group
        Group group1 = new Group();
        group1.id = 1;
        group1.name = "Testing System";

        Group group2 = new Group();
        group2.id = 2;
        group2.name = "Development";

        Group group3 = new Group();
        group3.id = 3;
        group3.name = "Sale";

        //Khởi tạo account
        Account account1 = new Account();
        account1.id = 1;
        account1.email = "ndao2k";
        account1.userName = "nam";
        account1.fullName = "Đào Xuân Nam";
        account1.department = department1;
        account1.position = position1;
        account1.createDate = LocalDate.now();
        Group[] groupAcc1 = {group1, group2};
        account1.groups = groupAcc1;

        Account account2 = new Account();
        account2.id = 2;
        account2.email = "ducthang99";
        account2.userName = "thang";
        account2.fullName = "Lưu Đức Thắng";
        account2.department = department2;
        account2.position = position2;
        account2.createDate = LocalDate.of(2021, 3, 4);
        account2.groups = new Group[]{group1, group2};

        Account account3 = new Account();
        account3.id = 3;
        account3.email = "annguyen99";
        account3.userName = "an";
        account3.fullName = "Nguyễn Tiến An";
        account3.department = department3;
        account3.position = position2;
        account3.createDate = LocalDate.now();

        group1.accounts = new Account[]{account1};
        group2.accounts = new Account[]{account1, account2};
        group3.accounts = new Account[]{account2};

    }
    public static void question1() {
        //question1
        System.out.println("------question1-----");
        if (account2.department == null) {
            System.out.println("Nhân viên này chưa có phòng ban.");
        } else {
            System.out.println("Phòng ban của nv này là: " +

                    account2.department.departmentName);
        }
    }

    public static void question2() {
        //question2
        System.out.println("---------Question 2---------");
        if (account2.groups == null) {
            System.out.println("Nhân viên này chưa có group");
        } else {
            int countGroup = account2.groups.length;
            if (countGroup == 1 || countGroup == 2) {
                System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
            }
            if (countGroup == 3) {
                System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
            }
            if (countGroup >= 4) {
                System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
            }
        }
    }

    public static void question3() {
        //question3
        System.out.println("---------Question 3---------");
        System.out.println(account2.department == null ? "Nhân viên này chưa có phòng ban." : "Phòng của nhân viên này là: "
                + account2.department.departmentName);
    }

    public static void question4() {
        //question4
        System.out.println("---------Question 4---------");
        System.out.println(account1.position.positionName.toString() ==
                "Dev" ? "Đây là Developer" : "Người này không phải là Developer");
    }

    public static void question5() {
        //question5
        System.out.println("---------Question 5---------");
        if (group1.accounts == null) {
            System.out.println("Group chưa có thành viên nào tham gia");
        } else {
            int countAccInGroup = group1.accounts.length;
            switch (countAccInGroup) {
                case 1:
                    System.out.println("Nhóm có một thành viên");
                    break;
                case 2:
                    System.out.println("Nhóm có hai thành viên");
                    break;
                case 3:
                    System.out.println("Nhóm có ba thành viên");
                    break;
                default:
                    System.out.println("Nhóm có nhiều thành viên");
                    break;
            }
        }
    }

    public static void question6() {
        //question6
        System.out.println("---------Question 6---------");
        if (account2.groups == null) {
            System.out.println("Nhân viên này chưa có group");
        } else {
            switch (account2.groups.length) {
                case 1:
                    System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
                    break;
                case 2:
                    System.out.println("Group của nhân viên này là Java Fresher, C# Fresher");
                    break;
                case 3:
                    System.out.println("Nhân viên này là người quan trọng, tham gia nhiều group");
                    break;
                default:
                    System.out.println("Nhân viên này là người hóng chuyện, tham gia tất cả các group");
                    break;
            }
        }
    }

    public static void question7() {
        //question7
        System.out.println("---------Question 7---------");
        String positionName = account1.position.positionName.toString();
        switch (positionName) {
            case "Dev":
                System.out.println("Đây là Developer");
                break;
            default:
                System.out.println("Người này không phải là Developer");
                break;
        }
    }

    public static void question8() {
        //question8
        System.out.println("---------Question 8---------");
        Account[] accArray = {account1, account2, account3};
        for (Account account : accArray) {
            System.out.println("AccountID: " + account.id + " Email: " +
                    account.email + " Name: " + account.fullName);
        }
    }

    public static void question9() {
        //question9
        System.out.println("---------Question 9---------");
        Department[] depArray = {department1, department2, department3};
        for (Department department : depArray) {
            System.out.println("DepID: " + department.departmentId + " Name: " + department.departmentName);
        }
    }

    public static void question10() {
        //question10
        System.out.println("---------Question 10---------");
        Account[] accArray1 = {account1, account2};
        for (int i = 0; i < accArray1.length; i++) {
            System.out.println("Thông tin account thứ " + (i + 1) + " là:");
            System.out.println("Email: " + accArray1[i].email);
            System.out.println("Full name: " + accArray1[i].fullName);
            System.out.println("Phòng ban: " + accArray1[i].department.departmentName);
        }
    }

    public static void question11() {
        //question11
        System.out.println("---------Question 11---------");
        Department[] depArray1 = {department1, department2, department3};
        for (int i = 0; i < depArray1.length; i++) {
            System.out.println("Thông tin department thứ " + (i + 1) + " là:");
            System.out.println("Id: " + depArray1[i].departmentId);
            System.out.println("Name: " + depArray1[i].departmentName);
        }
    }

    public static void question12() {
        //question12
        System.out.println("---------Question 12---------");
        Department[] depArray2 = {department1, department2, department3};
        for (int i = 0; i < 2; i++) {
            System.out.println("Thông tin department thứ " + (i + 1) + " là:");
            System.out.println("Id: " + depArray2[i].departmentId);
            System.out.println("Name: " + depArray2[i].departmentName);
        }
    }

    public static void question13() {
        //question13
        System.out.println("---------Question 13---------");
        Account[] accArray2 = {account1, account2, account3};
        for (int i = 0; i < accArray2.length; i++) {
            if (i != 1) {
                System.out.println("Thông tin account thứ " + (i + 1) + " là:");
                System.out.println("Email: " + accArray2[i].email);
                System.out.println("Full name: " + accArray2[i].fullName);
                System.out.println("Phòng ban: " + accArray2[i].department.departmentName);
            }
        }
    }

    public static void question14() {
        //question14
        System.out.println("---------Question 14---------");
        Account[] accArray3 = {account1, account2, account3};
        for (int j = 0; j < accArray3.length; j++) {
            if (accArray3[j].id < 4) {
                System.out.println("Thông tin account thứ " + (j + 1) + " là:");
                System.out.println("Email: " + accArray3[j].email);
                System.out.println("Full name: " + accArray3[j].fullName);
                System.out.println("Phòng ban: " + accArray3[j].department.departmentName);
            }
        }
    }

    public static void question15(){
        //question15
        System.out.println("---------Question 15---------");
        System.out.println("---------Question 15: In ra các số chẵn nhỏ hơn hoặc bằng 20-----------");
        for (int i = 1; i <= 20; i++) {
            if (i%2 ==0 ) {
                System.out.print(i+ " ");
            }
        }
    }

     */
}
