package com.vti.backend;
import com.vti.entity.*;

import java.time.LocalDate;
import java.util.Random;

public class Exercise4 {
    public static void question1(){
        System.out.println("-------question1-------");
        Random random= new Random();
        int n = random.nextInt();
        System.out.println("Số ngẫu nhiên là: "+n);
    }


    public static void question2(){
        System.out.println("-------question2-------");
        Random random= new Random();
        float n = random.nextFloat();
        System.out.println("Số ngẫu nhiên là: "+n);
    }

    public static void question3(){
        Random random= new Random();
        System.out.println("-------question3-------");
        String[] name= {"Đào Xuân Nam","Nguyễn Tiến An","Ngô Đức Mạnh"};
        int i=random.nextInt(name.length);
        System.out.println("Tên ngẫu nhiên là: "+name[i]);
    }

    public static void question4(){
        Random random= new Random();
        System.out.println("-------question4-------");
        int minDay= (int) LocalDate.of(1995,7,24).toEpochDay();
        int maxDay = (int) LocalDate.of(1995,12,20).toEpochDay();
        System.out.println("Ngày bắt đầu: "+minDay);
        System.out.println("Ngày kết thúc: "+maxDay);
        long randomInt = minDay+random.nextInt(maxDay-minDay);
        LocalDate randomDay= LocalDate.ofEpochDay(randomInt);
        System.out.println("Ngày ngẫu nhiên: "+randomDay);
    }

    public static void question5(){
        Random random= new Random();
        int now= (int) LocalDate.now().toEpochDay();
        long random1=random.nextInt(now);
        LocalDate localDate= LocalDate.ofEpochDay(random1);
        System.out.println("Ngày ngẫu nhiên trong quá khứ là: "+localDate);
    }

    public static void question6(){
        Random random= new Random();
        int maxDay = (int) LocalDate.now().toEpochDay();
        long randomDay = random.nextInt(maxDay);
        LocalDate localDate = LocalDate.ofEpochDay(randomDay);
        System.out.println("1 Ngày ngẫu nhiên trong quá khứ: " + localDate);
    }

    public static void question7(){
        Random random= new Random();
        int a= random.nextInt(900)+100;
        System.out.println(a);
    }

}
