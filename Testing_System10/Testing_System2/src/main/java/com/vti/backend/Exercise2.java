package com.vti.backend;
import com.vti.utils.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Exercise2 {
    public static void question1(){
        System.out.println("------question1------");
        int i=5;
        System.out.println(i);
    }

    public static void question2(){
        System.out.println("------question2------");
        int i1 = 100000000;
        System.out.printf(Locale.US, "%,d%n", i1);
    }

    public static void question3(){
        System.out.println("------question3------");
        float c = 5.567098f;
        System.out.printf("%5.4f%n", c);
    }

    public static void question4(){
        System.out.println("------question4------");
        String s = "Nguyễn Văn A";
        System.out.printf("Tên tôi là " + s + " và tôi đang độc thân");
    }

    public static void question5(){
        System.out.println("------question5------");
        String pattern = "dd/MM/yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        System.out.println(date);
    }
}
