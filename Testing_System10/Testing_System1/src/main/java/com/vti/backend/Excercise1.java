package com.vti.backend;

import com.vti.entity.*;

import java.time.LocalDate;

public class Excercise1 {
    public void question2(){
        //Khởi tạo departmen
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Marketing";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Sale";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "CSKH";

        //Khởi tạo Position
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.Dev;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.Test;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.ScrumMaster;

        Position position4 = new Position();
        position4.positionId = 4;
        position4.positionName = PositionName.PM;

        //Khởi tạo Group
        Group group1 = new Group();
        group1.id = 1;
        group1.name = "Testing System";

        Group group2 = new Group();
        group2.id = 2;
        group2.name = "Development";

        Group group3 = new Group();
        group3.id = 3;
        group3.name = "Sale";

        //Khởi tạo account
        Account account1 = new Account();
        account1.id = 1;
        account1.email = "ndao2k";
        account1.userName = "nam";
        account1.fullName = "Đào Xuân Nam";
        account1.department = department1;
        account1.position = position1;
        account1.createDate = LocalDate.now();
        Group[] groupAcc1 = { group1, group2 };
        account1.groups = groupAcc1;

        Account account2 = new Account();
        account2.id = 2;
        account2.email = "ducthang99";
        account2.userName = "thang";
        account2.fullName = "Lưu Đức Thắng";
        account2.department = department2;
        account2.position = position2;
        account2.createDate = LocalDate.of(2021,3,4);
        account2.groups = new Group[]{group1,group2};

        Account account3 = new Account();
        account3.id = 3;
        account3.email = "annguyen99";
        account3.userName = "an";
        account3.fullName = "Nguyễn Tiến An";
        account3.department = department3;
        account3.position = position2;
        account3.createDate = LocalDate.now();

        System.out.println("Thông tin account: ");
        System.out.println("Account1: ID "+account1.id+" "+
                "Email: "+account1.email+" "+
                "UserName: "+account1.userName+" "+
                "FullName: "+account1.fullName+" "+
                "Department: "+account1.department.departmentName+" "+
                "Position: "+account1.position.positionName+" "+
                "Group: "+account1.groups[0].name+" "+
                " "+account1.groups[1].name+" "+
                "CreateDate: "+account1.createDate);

        System.out.println("Account2: ID "+account2.id+""+
                "Email: "+account2.email+" "+
                "UserName: "+account2.userName+" "+
                "FullName: "+account2.fullName+" "+
                "Department: "+account2.department.departmentName+" "+
                "Position: "+account2.position.positionName+" "+
                "Group: "+account2.groups[0].name+" "+
                " "+account2.groups[1].name+" "+
                "CreateDate: "+account2.createDate);

        System.out.println("Account1: ID "+account3.id+" "+
                "Email: "+account3.email+" "+
                "UserName: "+account3.userName+" "+
                "FullName: "+account3.fullName+" "+
                "Department: "+account3.department.departmentName+" "+
                "Position: "+account3.position.positionName+" "+
                "Group: "+" "+
                "CreateDate: "+account3.createDate);
    }
}
