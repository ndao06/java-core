package com.vti.frontend;

import com.vti.backend.Exercise2_Ques1;
import com.vti.entity.Student1;
import com.vti.utils.ScannerUtils;

import java.util.ArrayList;
import java.util.List;

public class Program2_exe2_que1 {
    public static void main(String[] args) {
            List<Student1> student1List = new ArrayList<>();
            Exercise2_Ques1 exercise2 = new Exercise2_Ques1();
            while (true){
                System.out.println("-----------Lựa chọn chức năng bạn muốn sử dụng---------");
                System.out.println("        1: Tạo danh sách 10 sinh viên. ");
                System.out.println("        2: Hiển thị danh sách sinh viên. ");
                System.out.println("        3: Điểm danh lớp. ");
                System.out.println("        4: Gọi 1 nhóm đi học bài. ");
                System.out.println("        5: Gọi 1 nhóm đi dọn vệ sinh. ");
                System.out.println("        6: Thoát chương trình");
                System.out.println("-------------------------------------------------------");

                int chose= ScannerUtils.inputNumber();
                switch (chose){
                    case 1:
                        Student1 student1 = exercise2.SinhVien();
                        System.out.println("Tạo thành công 10 sinh viên.");
                        break;
                    case 2:
                        for(Student1 st: student1List){
                            System.out.println(st);
                        }
                        break;
                    case 3:
                        System.out.println("Cả lớp điểm danh: ");
                        for(Student1 st: student1List){
                            st.diemDanh();
                        }
                        break;
                    case 4:
                        System.out.println("Nhóm 1 đi học bài: ");
                        for(Student1 st: student1List){
                            if(st.getGroup() ==1){
                                st.hocBai();
                            }
                        }
                        break;
                    case 5:
                        System.out.println("Nhóm 2 đi dọn vệ sinh: ");
                        for(Student1 st: student1List){
                            if(st.getGroup() ==2){
                                st.donVS();
                            }
                        }
                        break;
                    case 6:
                        return;
                    default:
                        System.out.println("Chọn đúng số trên menu");
                        break;
                }
            }
        }
    }

