package com.vti.backend;

import com.vti.entity.HinhChuNhat;
import com.vti.entity.HinhVuong;
import com.vti.utils.ScannerUtils;

public class Exercise2_Ques3 {
    public HinhVuong hinhVuong;
    public HinhChuNhat hinhChuNhat;
    public void question(){
        while (true){
            System.out.println("-----------Lựa chọn chức năng bạn muốn sử dụng---------");
            System.out.println("        1: Tính chu vi và diện tích hình vuông . ");
            System.out.println("        2: Tính chu vi và diện tích hình chữ nhật. ");
            System.out.println("        3: Thoát chương trình");
            System.out.println("-------------------------------------------------------");

            int chose = ScannerUtils.inputNumber();
            switch (chose){
                case 1:
                    System.out.println("Nhập vào cạnh hình vuông: ");
                    float d = ScannerUtils.inputNumberFloat();
                    hinhVuong = new  HinhVuong(d);
                    System.out.println("Hình vuông có cạnh là: "+d+"" +
                            "có chu vi là: "+ hinhVuong.tinhChuVi()+" " +
                            "có diện tích là: "+hinhVuong.tinhDienTich());
                    break;
                case 2:
                    System.out.println("Nhập vào chiều dài của hình chữ nhật: ");
                    float e = ScannerUtils.inputNumberFloat();
                    System.out.println("Nhập vào chiều rộng của hình chữ nhật: ");
                    float t = ScannerUtils.inputNumberFloat();
                    hinhChuNhat = new HinhChuNhat(e,t);
                    System.out.println("Hình chữ nhật có cạnh là: "+e+"" +
                            "có chu vi là: "+ hinhChuNhat.tinhChuVi()+" " +
                            "có diện tích là: "+hinhChuNhat.tinhDienTich());
                    break;
                case 3:
                    return;
                default:
                    System.out.println("nhập đúng số trên. ");
                    break;
            }
        }
    }

}
