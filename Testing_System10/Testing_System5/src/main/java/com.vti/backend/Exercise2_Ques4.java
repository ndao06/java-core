package com.vti.backend;

import com.vti.entity.MyMath;
import com.vti.utils.ScannerUtils;

import java.util.Scanner;

public class Exercise2_Ques4 {
    public void question(){
        MyMath myMath = new MyMath();
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("------------Lựa chọn chức năng bạn muốn-----------");
            System.out.println("         1: Tính tổng 2 số kiểu int. ");
            System.out.println("         2: Tính tổng 2 số kiểu byte. ");
            System.out.println("         3: Tính tổng 2 số kiểu Float. ");
            System.out.println("         4: Tính tổng 2 số kiểu int và float. ");
            System.out.println("         5: Tính tổng 2 số kiểu float và byte. ");
            System.out.println("         6: Thoát chương trình. ");
            System.out.println("--------------------------------------------------");

            int chose = ScannerUtils.inputNumber();
            switch (chose){
                case 1:
                    System.out.println("Nhập vào số int thứ nhất: ");
                    int int1 = ScannerUtils.inputNumber();
                    System.out.println("Nhập vào số int thứ nhất: ");
                    int int2 = ScannerUtils.inputNumber();
                    System.out.println("Tổng 2 số int vừa nhập là "+myMath.getSum(int1,int2));
                    break;
                case 2:
                    System.out.println("Nhấp vào sô byte thứ nhất: ");
                    byte byte1 = scanner.nextByte();
                    System.out.println("Nhấp vào sô byte thứ nhất: ");
                    byte byte2 = scanner.nextByte();
                    System.out.println("Tổng 2 số vừa nhập là: "+(myMath.getSum(byte1,byte2)));
                    break;
                case 3:
                    System.out.println("Nhập vào số float 1: ");
                    float float1 = ScannerUtils.inputNumberFloat();
                    System.out.println("Nhập vào số float 2: ");
                    float float2 = ScannerUtils.inputNumberFloat();
                    System.out.println("Tổng 2 số là: " +(myMath.getSum(float1,float2)));
                    break;
                case 4:
                    System.out.println("Nhập vào số int 1: ");
                    int int3 = ScannerUtils.inputNumber();
                    System.out.println("Nhập vào số float 2: ");
                    float float3 = ScannerUtils.inputNumberFloat();
                    System.out.println("Tổng 2 số là: " + (myMath.getSum(float3,int3)));
                    break;
                case 5:
                    System.out.println("Nhập vào số byte 1: ");
                    byte byte3 = scanner.nextByte();
                    System.out.println("Nhập vào số float 2: ");
                    float float4 = ScannerUtils.inputNumberFloat();
                    System.out.println("Tổng 2 số là: " + (myMath.getSum(byte3,float4)));
                    break;
                case 6:
                    return;
            }
            }
        }

}

