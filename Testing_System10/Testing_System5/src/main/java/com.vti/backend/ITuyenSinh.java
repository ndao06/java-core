package com.vti.backend;

import com.vti.entity.Student;

public interface ITuyenSinh {
    void addNewsStudent();

    void viewStudent();

    Student findByStudentCode(String studentCode);
}
