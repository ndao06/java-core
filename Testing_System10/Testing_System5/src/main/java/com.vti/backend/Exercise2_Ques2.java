package com.vti.backend;

import com.vti.entity.Person;
import com.vti.entity.Student_Person;
import com.vti.utils.ScannerUtils;

public class Exercise2_Ques2 {
    public void question2(){
        while (true){
            System.out.println("-----------Lựa chọn chức năng bạn muốn sử dụng---------");
            System.out.println("        1: Nhập và in Person. ");
            System.out.println("        2: Nhập và in Student. ");
            System.out.println("        3: Thoát chương trình");
            System.out.println("-------------------------------------------------------");

            int chose = ScannerUtils.inputNumber();
            switch (chose){
                case 1:
                    student();
                    break;
                case 2:
                    person();
                    break;
                case 3:
                    return;
                default:
                    System.out.println("nhập đúng số trên. ");
                    break;
            }
        }
    }

    private void student(){
        Student_Person student_person = new Student_Person();
        student_person.inputInfo();
        System.out.println("Thông tin Student vừa nhập: ");
        System.out.println(student_person.showInf());
        if(student_person.hocBong()){
            System.out.println("Student này đạt học bổng");
        }else {
            System.out.println("Student này không đạt học bổng");
        }
    }

    private void person(){
        Person person = new Person();
        person.inputInfo();
        System.out.println("Thông tin Person vừa nhập: ");
        System.out.println(person.showInf());
    }
}
