package com.vti.entity;

public class HinhChuNhat {
    public HinhChuNhat(float a, float b) {

        this.a = a;
        this.b = b;
    }

    private float a;
    private float b;

    public  float tinhChuVi(){
        return 2 *(a+b);
    }

    public  float tinhDienTich(){
        return a*b;
    }


}
