package com.vti.entity;

import com.vti.utils.ScannerUtils;

import java.util.Scanner;

public class News implements INews {
    private int id;

    private String title;

    @Override
    public String toString() {
        return "INews{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pubLishDate='" + pubLishDate + '\'' +
                ", Author='" + Author + '\'' +
                ", Content='" + content + '\'' +
                ", AverageRate=" + averageRate +
                '}';
    }

    private String pubLishDate;

    private String Author;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubLishDate() {
        return pubLishDate;
    }

    public void setPubLishDate(String pubLishDate) {
        this.pubLishDate = pubLishDate;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        content = content;
    }

    public float getAverageRate() {
        return averageRate;
    }



    public void Display(){
        System.out.println(title);
        System.out.println(this.pubLishDate);
        System.out.println(this.content);
        System.out.println(this.Author);
        System.out.println(this.averageRate);
    }


    private String content;

    private float averageRate;

    @Override
    public void display() {

    }

    @Override
    public float culCudate() {
        System.out.println("Mời bạn đánh giá");
        Scanner scanner= new Scanner(System.in);
        int[] rates =new int[3];
        rates[0]= ScannerUtils.inputNumber(1,5);
        rates[1]= ScannerUtils.inputNumber(1,5);
        rates[2]= ScannerUtils.inputNumber(1,5);
        this.averageRate = (rates[0]+rates[1]+rates[3])/3.0f;
        return averageRate;
    }
}
