package com.vti.entity;

public class MyMath {
    public int getSum(int par1, int par2) {
        return (par1 + par2);
    }
    public byte getSum(byte par1, byte par2) {
        return (byte) (par1 + par2);
    }
    public Float getSum(float par1, float par2) {
        return (par1 + par2);
    }

    public Float getSum(byte par1, float par2) {
        return (par1 + par2);
    }
    public int getSum(byte par1, int par2) {
        return (par1 + par2);
    }
    public Float getSum(float par1, int par2) {
        return (par1 + par2);
    }
}
