package com.vti.entity;

import com.vti.utils.ScannerUtils;

public class Student_Person extends Person{
    private int id;

    @Override
    public String toString() {
        return "Student_Person{" +
                "id=" + id +
                ", avg=" + avg +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public void inputInfo() {
        super.inputInfo();
        System.out.println("Nhập ID: ");
        this.id = ScannerUtils.inputNumber();
        System.out.println("Nhập điểm trung bình: ");
        this.avg = ScannerUtils.inputNumberFloat();
        System.out.println("Nhập Email: ");
        this.email= ScannerUtils.inputEmail();
    }

    @Override
    public String showInf() {
        return super.showInf()+"id=" + id +
                ", avg=" + avg +
                ", email='" + email + '\'' +
                '}';
    }

    public boolean hocBong(){
        return avg >8 ? true:false;
    }

    private float avg;
    private String email;


}
