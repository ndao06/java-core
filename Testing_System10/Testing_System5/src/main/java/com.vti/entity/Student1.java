package com.vti.entity;

public class Student1 implements IStudent {
    protected int id;


    public Student1() {
    }

    public Student1(int id, String name, int group) {
        this.id = id;
        this.name = name;
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getGroup() {
        return group;
    }

    private String name;

    @Override
    public String toString() {
        return "Student1{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", group=" + group +
                '}';
    }

    private int group;

    @Override
    public void diemDanh() {
        System.out.println(this.name + "điểm danh");
    }

    @Override
    public void hocBai() {
        System.out.println(this.name + "học bài");
    }

    @Override
    public void donVS() {
        System.out.println(this.name + "đang dọn vệ sinh");
    }

}
