package com.vti.entity;

import com.vti.utils.ScannerUtils;

import java.time.LocalDate;
import java.util.Scanner;

public class Person {
    public Person() {
    }



    private String name;
    private Gender gender;
    public Person(String name, Gender gender, LocalDate date, String address) {
        this.name = name;
        this.gender = gender;
        this.date = date;
        this.address = address;
    }

    public void inputInfo(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập thông tin Person từ bàn phím: ");
        System.out.println("Nhập tên: ");
        this.name= ScannerUtils.inputString();
        System.out.println("Nhập giới tính: " +
                "1: Nam" +
                "2: Nữ" +
                "3: Khác");
        int a= ScannerUtils.inputNumber();
        switch (a){
            case 1:
                this.gender= Gender.MALE;
                break;
            case 2:
                this.gender=Gender.FEMALE;
                break;
            case 3:
                this.gender=Gender.UNKNOWN;
                break;
        }
        System.out.println("Nhập ngày sinh theo định dạng yyyy-MM-dd: ");
        this.date=LocalDate.parse(scanner.next());
        System.out.println("Địa chỉ: ");
        this.address= ScannerUtils.inputString();
    }

    public String showInf(){
        return "Person{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", date=" + date +
                ", address='" + address + '\'' +
                '}';
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", date=" + date +
                ", address='" + address + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private LocalDate date;
    private String address;


}
