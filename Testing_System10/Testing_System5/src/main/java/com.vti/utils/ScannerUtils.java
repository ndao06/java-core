package com.vti.utils;

import java.util.Scanner;

public class ScannerUtils {
        static Scanner scanner = new Scanner(System.in);

        public static String inputString(){
            return scanner.nextLine();
        }

        public static int inputNumber(){
            int number = Integer.parseInt(scanner.nextLine());
            while (number<=0){
                System.out.println("Mời bạn nhập lại: ");
                number=Integer.parseInt(scanner.nextLine());
            }
            return number;
        }

    public static float inputNumberFloat(){
        int number = Integer.parseInt(scanner.nextLine());
        while (number<=0){
            System.out.println("Mời bạn nhập lại: ");
            number=Integer.parseInt(scanner.nextLine());
        }
        return number;
    }
}
