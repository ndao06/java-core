package com.vti.backend;

import com.vti.utils.ScannerUtils;

import java.util.Scanner;

public class Exercise1 {
    private double diemtoan;

    private double diemhoa;

    private double diemly;

    private double diemtb;

    public void question1(){
        System.out.println("Nhập 1 số thực: ");
        int number = ScannerUtils.inputNumber();
        double inch = (double) number*2.54;
        double foot = (double) inch*12;
        System.out.println("Số thực vừa nhập là: "+number+" cm"+ "tương đương với: "+inch+"inch"+" và tương đương: "+foot);
    }

    public void question2(){
        System.out.println("Nhập vào 1 số ngẫu nhiên từ 0 -> 68399: ");
        int number = ScannerUtils.inputNumber();
        int h,m,s;
        if(0< number && number<68399){
            h= number/60;
        }
    }

    public void question3(){
        System.out.println("Nhập số nguyên thứ nhất: ");
        int number1 = ScannerUtils.inputNumber();
        System.out.println("Nhập số nguyên thứ hai: ");
        int number2 = ScannerUtils.inputNumber();
        System.out.println("Nhập số nguyên thứ ba: ");
        int number3 = ScannerUtils.inputNumber();
        System.out.println("Nhập số nguyên thứ bốn: ");
        int number4 = ScannerUtils.inputNumber();

        int[] array = {number1,number2,number3,number4};
        int max=number1,min=number1;

        for (int i = 0; i <array.length ; i++) {
            if(array[i]>max){
                max = array[i];
            }
            System.out.println("Số lớn nhất là: "+max);
        }

        for (int i = 0; i <array.length ; i++) {
            if(array[i]<min){
                min = array[i];
            }
            System.out.println("Số lớn nhất là: "+min);
        }
    }

    public void question4(){
        System.out.println("Nhập số nguyên thứ nhất: ");
        int number1 = ScannerUtils.inputNumber();
        System.out.println("Nhập số nguyên thứ hai: ");
        int number2 = ScannerUtils.inputNumber();

        int hieu=(number1-number2);
        System.out.println("Hiệu hai số là: "+hieu);
        if(hieu>0){
            System.out.println("Số thứ nhất lớn hơn số thứ hai");
        }else {
            System.out.println("Số thứ nhất nhỏ hơn số thứ hai");
        }
    }

    public void question5(){
        System.out.println("Nhập số  thứ nhất: ");
        int number1 = ScannerUtils.inputNumber();
        System.out.println("Nhập số  thứ hai: ");
        int number2 = ScannerUtils.inputNumber();

        if(number1%number2==0){
            System.out.println("Số thứ nhất chia hết cho số thứ hai");
        }else {
            System.out.println("Số thứ nhất không chia hết cho số thứ hai");
        }
    }

    public void question6(){
        while (true) {
            System.out.println("-------------Mời bạn nhập chương trình------------");
            System.out.println("           1: Nhập vào điểm 3 môn học.");
            System.out.println("           2: Tính điếm trung bình.");
            System.out.println("           3: Xác định học lực của học sinh dựa trên điểm trung bình.");
            System.out.println("           4: Hiển thị học lực của học sinh.");
            System.out.println("           5:Thoát chương trình.");

            int chose = ScannerUtils.inputNumber();

            switch (chose){
                case 1:
                    diem();
                    break;
                case 2:
                    diemtrungbinh();
                    System.out.println("Điểm trung bình là: "+diemtrungbinh());
                    break;
                case 3:
                    hocluc();
                    break;
                case 4:
                    System.out.println("Học sinh có học lực là: ");
                    hocluc();
                    break;
                case 5:
                    return;
                default:
                    System.out.println("Mời nhập đúng");
                    break;
            }
        }
    }
    public void diem(){
        Scanner scanner= new Scanner(System.in);
        do {
            System.out.println("Mời nhập điểm toán: ");
            diemtoan = scanner.nextDouble();
            System.out.println("Mời nhập điểm lý: ");
            diemly = scanner.nextDouble();
            System.out.println("Mời nhập điểm hóa: ");
            diemhoa = scanner.nextDouble();
        }while ((diemtoan<=0 && diemtoan>=10) && (diemhoa<=0 && diemhoa>=10)&& (diemly<=0 && diemly>=10));
    }

    public double diemtrungbinh(){
      return  diemtb = ((diemtoan)*2+diemly+diemhoa)/4;
    }

    public void hocluc(){
        if(diemtb>=9){
            System.out.println("Loại xuất sắc");
        }else if(diemtb>=8 && diemtb <9){
            System.out.println("Loại giỏi");
        }else if (diemtb>=7 && diemtb<8){
            System.out.println("Loại khá");
        }else if(diemtb>=6 && diemtb<7){
            System.out.println("Loại trung bình khá");
        }else if(diemtb>=5 && diemtb<6){
            System.out.println("Loại trung bình");
        }else {
            System.out.println("Loại kém");
        }
    }
}
