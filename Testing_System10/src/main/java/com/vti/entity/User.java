package com.vti.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@ToString
@Setter
public class User {
    private  int id;
    private Role role;
    private String username;
    private String password;
    private String email;
    public String date;
    private Department department;
}
