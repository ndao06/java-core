package com.vti.backend.controller;

import com.vti.entity.Account;
import com.vti.backend.service.AccountService;

import java.util.List;

public class AccountController {
    AccountService accountService = new AccountService();

    public void createAccount(String username, String email, String password){
        accountService.createAccount(username,email,password);
    }

    public void updateAccount(int id, String odlPassword, String newPassword) {
        accountService.updateAccount(id, odlPassword, newPassword);
    }


    public void deleteAccount(int accountID) {
        accountService.deleteAccount(accountID);
    }


    public List<Account> findByEmail(String email) {
        return accountService.findByEmail(email);
    }


    public List<Account> getAllAccount() {
        return accountService.getAllAccount();
    }


    public boolean login(String email, String password) {
        return accountService.login(email,password);
    }
}
