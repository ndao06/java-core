package com.vti.backend.controller;

import com.vti.backend.service.AdminService;
import com.vti.entity.Department;
import com.vti.entity.User;

import java.util.List;

public class AdminController {
    AdminService adminService = new AdminService();

    public List<User> getAllUser() {
        return adminService.getAllUser();
    }

    public void deleteUser(int id) {
        adminService.deleteUser(id);
    }

    public void changePassword(int id, String oldPassword, String newPassword) {
        adminService.changePassword(id,oldPassword,newPassword);
    }

    public void addUser(String username, String email, String date, int id) {
        adminService.addUser(username, email, date, id);
    }

    public List<Department> getAllDepartment() {
        return adminService.getAllDepartment();
    }

    public void deleteDepartment(int id) {
        adminService.deleteDepartment(id);
    }

    public void updateDepartmentName(int id, String oldName, String newName) {
        adminService.updateDepartmentName(id, oldName, newName);
    }
}
