package com.vti.backend.controller;

import com.vti.entity.Department;
import com.vti.entity.User;
import com.vti.backend.service.UserService;

import java.util.List;

public class UserController {
    UserService userService = new UserService();

    public User login(String username, String password) {
        return userService.login(username,password);
    }

    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    public void findById(int id) {
        userService.findById(id);
    }

    public List<User> findByUserEmail(String word) {
        return userService.findByUserEmail(word);
    }

    public List<Department> getAllDepartment() {
        return userService.getAllDepartment();
    }

    public void findByDepartment(int id) {
        userService.findByDepartmentId(id);
    }

    public void findByDepartmentName(String name) {
        userService.findByDepartmentName(name);
    }
}
