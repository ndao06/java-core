package com.vti.backend.repository;

import com.vti.entity.Department;
import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AdminRepository {
    public List<User> getAllUser(){
        List<User> userList = new ArrayList<>();
        String sql = "select * from assignment10.User left join Department D on D.department_id = User.department_id";
        Connection connection = JdbcUtils.getConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                User user= new User();
                Department department = new Department();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDate(resultSet.getString("date_of_birth"));

                department.setDepartmentName(resultSet.getString("department_name"));
                user.setDepartment(department);
                userList.add(user);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
        return userList;
    }

    public void deleteUser(int id){
        String sql ="select * from assignment10.User where id =?";
        Connection connection = JdbcUtils.getConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);

            int resultSet = preparedStatement.executeUpdate();

            if (resultSet==0){
                System.out.println("Xóa không thành công.");
            }else {
                System.out.println("Xóa thành công");
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }
    }

    public void changePassword(int id,String oldPassword, String newPassword){
        String sql = "UPDATE assignment10.User SET password = ? WHERE id =? and password = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,newPassword);
            preparedStatement.setInt(2,id);
            preparedStatement.setString(3,oldPassword);

            int resultSet = preparedStatement.executeUpdate();

            if (resultSet==0){
                System.out.println("Thay đổi  password thất bại, không tìm thấy id hoặc password sai");
            }else {
                System.out.println("Thay đổi  password thành công");
            }

            JdbcUtils.closeConnection();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void addUser(String username,String email,String date, int id){
        String sql= "INSERT INTO assignment10.User (`role`, user_name, password, email, date_of_birth," +
                " department_id)\n" +
                "VALUES (2, ?, 123456, ?, ?, ?)";

        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,email);
            preparedStatement.setString(3,date);
            preparedStatement.setInt(4,id);

            int resultSet = preparedStatement.executeUpdate();
            if (resultSet==0){
                System.out.println("Thêm mới thất bại.");
            }else {
                System.out.println("Thêm mới thành công.");
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }
    }

    public List<Department> getAllDepartment(){
        List<Department> departmentList = new ArrayList<>();
        String sql = "select * from assignment10.Department";
        Connection connection = JdbcUtils.getConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                Department department = new Department();

                int id = resultSet.getInt("department_id");
                department.setId(id);
                department.setDepartmentName(resultSet.getString("department_name"));
                departmentList.add(department);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
        return departmentList;
    }

    public void deleteDepartment(int id){
        String sql ="select * from assignment10.Department where id =?";
        Connection connection = JdbcUtils.getConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);

            int resultSet = preparedStatement.executeUpdate();

            if (resultSet==0){
                System.out.println("Id department không tồn tại.");
            }else {
                System.out.println("Xóa thành công");
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }
    }

    public void updateDepartmentName(int id,String oldName, String newName){
        String sql = "UPDATE assignment10.Department SET department_name = ? WHERE department_id = ? AND department_name = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,newName);
            preparedStatement.setInt(2,id);
            preparedStatement.setString(3,oldName);

            int resultSet = preparedStatement.executeUpdate();

            if (resultSet==0){
                System.out.println("Update department thất bại");
            }else {
                System.out.println("Update department thành công");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }
    }
}
