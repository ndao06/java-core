package com.vti.backend.repository;

import com.vti.entity.Account;
import com.vti.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountRepository {
    public void createAccount(String username, String email, String password){
        // Tạo 1 câu query -> tương ứng với 1 chức năng muốn ử dụng
        String sql ="INSERT INTO jdbc.Account(full_name,email,password) VALUE(?,?,?)";
        //Kết nối tới database -> tạo 1 phiên làm việc
        Connection connection = JdbcUtils.getConnection();

        // Tạo 1 statement tương ứng với query( có biến truyền vào: PreparedStatement
        //Không có biến truyền vào thì dùng :Statement
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,email);
            preparedStatement.setString(3,password);

            // Execute câu query -> lấy kết quả (result)
            int resultSet = preparedStatement.executeUpdate();

            //Kiểm tra sự thành công và thông báo
            if(resultSet == 0){
                System.err.println("Thêm mới thất bại.");
            } else {
                System.out.println("Thêm mới thành công.");
            }
            JdbcUtils.closeConnection();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public void updateAccount(int id, String odlPassword, String newPassword){
        String sql = "UPDATE jdbc.Account SET password = ? WHERE account_id = ? AND password = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,newPassword);
            preparedStatement.setInt(2,id);
            preparedStatement.setString(3,odlPassword);

            int resultSet = preparedStatement.executeUpdate();

            if (resultSet==0){
                System.out.println("Update password thất bại");
            }else {
                System.out.println("Update password thành công");
            }

            JdbcUtils.closeConnection();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    public void deleteAccount(int accountID){
        String sql = "DELETE  FROM jdbc.Account WHERE (account_id = ?); ";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,accountID);

            int resultSet = preparedStatement.executeUpdate();
            if (resultSet ==0){
                System.out.println("Xóa không thành công");
            }else {
                System.out.println("Xóa thành công");
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
    }

    public List<Account> findByEmail(String email){
        String sql ="SELECT * FROM jdbc.Account WHERE email LIKE ?";

        Connection connection = JdbcUtils.getConnection();
        String words = "%" + email +"%";
        List<Account> accountList = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,words);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Account account= new Account();
                int accountID = resultSet.getInt("account_id");
                account.setAccountId(accountID);
                account.setFullName(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));
                accountList.add(account);
            }
            JdbcUtils.closeConnection();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return accountList;
    }


    public List<Account> getAllAccount(){
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM jdbc.Account";
        Connection connection = JdbcUtils.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                //Lấy giá trị từng hàng gán vào đổi tượng  account tương ứng
                Account account = new Account();

                int accountID= resultSet.getInt("account_id");
                account.setAccountId(accountID);

                account.setFullName(resultSet.getString("full_name"));
                account.setEmail(resultSet.getString("email"));
                account.setPassword(resultSet.getString("password"));

                accounts.add(account);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return accounts;
    }

    public boolean login(String email, String password){
        String sql ="SELECT * FROM jdbc.Account WHERE email = ? AND password = ?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,email);
            preparedStatement.setString(2,password);

            ResultSet resultSet = preparedStatement.executeQuery();

            return resultSet.next();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        finally {
            JdbcUtils.closeConnection();
        }
    }
}
