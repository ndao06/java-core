package com.vti.backend.repository;

import com.vti.entity.Department;
import com.vti.entity.Role;
import com.vti.entity.User;
import com.vti.utils.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class UserRepository {
    public User login(String username, String password){
        try {
            String sql ="SELECT * FROM assignment10.User WHERE user_name= ? AND password = ?";
            Connection connection= JdbcUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,password);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                User user = new User();
                user.setUsername(username);
                user.setEmail(resultSet.getString("email"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                return user;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }
        finally {
            JdbcUtils.closeConnection();
        }
        return null;
    }

    public List<User> getAllUser(){
        List<User> userList = new ArrayList<>();
        String sql = "select * from assignment10.User left join Department D on D.department_id = User.department_id";
        Connection connection = JdbcUtils.getConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                User user= new User();
                Department department = new Department();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDate(resultSet.getString("date_of_birth"));

                department.setDepartmentName(resultSet.getString("department_name"));
                user.setDepartment(department);
                userList.add(user);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
        return userList;
    }

    public void findById(int id){
        String sql = "select * from assignment10.User where id =?";
        Connection connection= JdbcUtils.getConnection();

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                int id1 = resultSet.getInt("id");
                String roleString = resultSet.getString("role");
                String username = resultSet.getString("user_name");
                String email = resultSet.getString("email");
                String date = resultSet.getString("date_of_birth");

                System.out.println("id: "+id1);
                System.out.println("role: "+roleString);
                System.out.println("user_name: "+username);
                System.out.println("email: "+email);
                System.out.println("date_of_birth: "+date);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
    }

    public List<User> findByUserEmail(String word){
        List<User> userList = new ArrayList<>();
        String sql = "select * from assignment10.User where email like ? or user_name like ?";

        Connection connection = JdbcUtils.getConnection();
        String convert = "%"+word+"%";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,convert);
            preparedStatement.setString(2,convert);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                User user= new User();
                user.setId(resultSet.getInt("id"));
                String roleString = resultSet.getString("role");
                user.setRole(Role.valueOf(roleString));
                user.setUsername(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("email"));
                user.setDate(resultSet.getString("date_of_birth"));

                userList.add(user);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
        return userList;
    }

    public List<Department> getAllDepartment(){
        List<Department> departmentList = new ArrayList<>();
        String sql = "select * from assignment10.Department";
        Connection connection = JdbcUtils.getConnection();

        try{
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                Department department = new Department();

                int id = resultSet.getInt("department_id");
                department.setId(id);
                department.setDepartmentName(resultSet.getString("department_name"));
                departmentList.add(department);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        finally {
            JdbcUtils.closeConnection();
        }
        return departmentList;
    }

    public void findByDepartmentId(int id){
        String sql ="select * from assignment10.Department where department_id =?";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                int id1 = resultSet.getInt("department_id");
                String departmentName = resultSet.getString("department_name");


                System.out.println("id: "+id1);
                System.out.println("role: "+departmentName);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }
    }

//    public Department a(int x){
//        String sql ="select * from assignment10.Department where department_id =?";
//        Connection connection = JdbcUtils.getConnection();
//
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(sql);
//            preparedStatement.setInt(1,x);
//
//            ResultSet resultSet = preparedStatement.executeQuery();
//
//            if (resultSet.next()){
//                Department department = new Department(resultSet.getInt("department_id"),
//                        "resultSet.getString("department_name")");
//
//            }
//        }catch (Exception e){
//            System.err.println(e.getMessage());
//        }finally {
//            JdbcUtils.closeConnection();
//        }
//
//    }

    public void findByDepartmentName(String name){
        String sql ="select * from assignment10.Department where department_name  like ?";
        String convert = "%"+name+"%";
        Connection connection = JdbcUtils.getConnection();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,convert);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                int id1 = resultSet.getInt("department_id");
                String departmentName = resultSet.getString("department_name");


                System.out.println("id: "+id1);
                System.out.println("role: "+departmentName);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }finally {
            JdbcUtils.closeConnection();
        }
    }
}
