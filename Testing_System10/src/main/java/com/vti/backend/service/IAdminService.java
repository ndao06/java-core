package com.vti.backend.service;

import com.vti.entity.Department;
import com.vti.entity.User;

import java.util.List;

public interface IAdminService {
    List<User> getAllUser();

    void deleteUser(int id);

    void changePassword(int id,String oldPassword, String newPassword);

    void addUser(String username,String email,String date, int id);

    List<Department> getAllDepartment();

    void deleteDepartment(int id);

    void updateDepartmentName(int id,String oldName, String newName);
}
