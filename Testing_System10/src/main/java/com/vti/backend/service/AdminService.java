package com.vti.backend.service;

import com.vti.backend.repository.AdminRepository;
import com.vti.entity.Department;
import com.vti.entity.User;

import java.util.List;

public class AdminService implements IAdminService{
    AdminRepository adminRepository = new AdminRepository();
    @Override
    public List<User> getAllUser() {
        return adminRepository.getAllUser();
    }

    @Override
    public void deleteUser(int id) {
        adminRepository.deleteUser(id);
    }

    @Override
    public void changePassword(int id, String oldPassword, String newPassword) {
        adminRepository.changePassword(id,oldPassword,newPassword);
    }

    @Override
    public void addUser(String username, String email, String date, int id) {
        adminRepository.addUser(username, email, date, id);
    }

    @Override
    public List<Department> getAllDepartment() {
        return adminRepository.getAllDepartment();
    }

    @Override
    public void deleteDepartment(int id) {
        adminRepository.deleteDepartment(id);
    }

    @Override
    public void updateDepartmentName(int id, String oldName, String newName) {
        adminRepository.updateDepartmentName(id, oldName, newName);
    }
}
