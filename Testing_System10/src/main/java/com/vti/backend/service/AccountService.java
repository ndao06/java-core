package com.vti.backend.service;

import com.vti.backend.repository.AccountRepository;
import com.vti.entity.Account;

import java.util.List;

public class AccountService implements IAccountService {
    private AccountRepository repository = new AccountRepository();
    @Override
    public void createAccount(String username, String email, String password) {
        repository.createAccount(username,email,password);
    }

    @Override
    public void updateAccount(int id, String odlPassword, String newPassword) {
        repository.updateAccount(id, odlPassword, newPassword);
    }

    @Override
    public void deleteAccount(int accountID) {
        repository.deleteAccount(accountID);
    }

    @Override
    public List<Account> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public List<Account> getAllAccount() {
        return repository.getAllAccount();
    }

    @Override
    public boolean login(String email, String password) {
        return repository.login(email,password);
    }
}
