package com.vti.backend.service;

import com.vti.backend.repository.UserRepository;

import com.vti.entity.Department;
import com.vti.entity.User;

import java.util.List;

public class UserService implements IUserService {
    UserRepository userRepository = new UserRepository();


    @Override
    public User login(String username, String password) {
        return userRepository.login(username,password);
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.getAllUser();
    }

    @Override
    public void findById(int id) {
        userRepository.findById(id);
    }

    @Override
    public List<User> findByUserEmail(String word) {
        return userRepository.findByUserEmail(word);
    }

    @Override
    public List<Department> getAllDepartment() {
        return userRepository.getAllDepartment();
    }

    @Override
    public void findByDepartmentId(int id) {
        userRepository.findByDepartmentId(id);
    }

    @Override
    public void findByDepartmentName(String name) {
        userRepository.findByDepartmentName(name);
    }


}
