package com.vti.backend.service;

import com.vti.entity.Account;

import java.util.List;

public interface IAccountService {
    void createAccount(String username, String email, String password);

    void updateAccount(int id, String odlPassword, String newPassword);

    void deleteAccount(int accountID);

    List<Account> findByEmail(String email);

    List<Account> getAllAccount();

    boolean login(String email, String password);
}
