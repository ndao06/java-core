package com.vti.backend.service;

import com.vti.entity.Department;
import com.vti.entity.User;

import java.util.List;

public interface IUserService {
    User login(String username, String password);

    List<User> getAllUser();

    void findById(int id);

    List<User> findByUserEmail(String word);

    List<Department> getAllDepartment();

    void findByDepartmentId(int id);

    void findByDepartmentName(String name);
}
