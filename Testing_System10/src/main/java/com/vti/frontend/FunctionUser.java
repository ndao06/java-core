package com.vti.frontend;

import com.vti.backend.controller.UserController;
import com.vti.entity.Department;
import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

import java.util.ArrayList;
import java.util.List;

public class FunctionUser {
    UserController userController = new UserController();
    public User login(){
        System.out.println("Mời bạn nhập username:");
        String username= ScannerUtils.inputString();
        String password = ScannerUtils.inputPassword();
        return userController.login(username,password);
    }

    public void getAllUser(){
        List<User> userList = userController.getAllUser();
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user: userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                    user.getDate(), user.getDepartment().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }

    public void findById(){
        System.out.println("Mời bạn nhập ID của user cần tìm: ");
        int id = ScannerUtils.inputNumber();

        userController.findById(id);
    }

    public void findByUserEmail(){
        System.out.println("Mời bạn nhập từ khóa để tìm kiếm user: ");
        String word = ScannerUtils.inputString();
        List<User> userList = userController.findByUserEmail(word);
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+%n");
        for (User user: userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                    user.getDate());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }

    public void getAllDepartment(){
        List<Department> departmentList = userController.getAllDepartment();
        String leftAlignFormat = "| %-3s| %-15s |%n";
        System.out.format("+----+-----------------+%n");
        System.out.format("| id | department_name |%n");
        System.out.format("+----+-----------------+%n");
        for (Department department: departmentList) {
            System.out.format(leftAlignFormat,department.getId(),department.getDepartmentName());
        }
        System.out.format("+----+-----------------+%n");
    }

    public void findByDepartmentId(){
        System.out.println("Nhập id của Department cần tìm: ");
        int id = ScannerUtils.inputNumber();

        userController.findByDepartment(id);
    }

    public void findByDepartmentName(){
        System.out.println("Tìm kiếm department theo name, mời nhập :");
        String name = ScannerUtils.inputString();

        userController.findByDepartmentName(name);
    }
}

