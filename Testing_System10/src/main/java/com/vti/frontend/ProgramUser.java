package com.vti.frontend;


import com.vti.utils.ScannerUtils;

public class ProgramUser {
    static FunctionUser functionUser = new FunctionUser();
    static FunctionAdmin functionAdmin = new FunctionAdmin();
//    public static void main(String[] args) {
//        UserController userController= new UserController();
//
//        System.out.println("---------------------Đăng Nhập------------------");
//        while (true){
//            User user = function1.login();
//            if(user== null){
//                System.err.println("User hoặc password không đúng, mời nhập lại");
//            }else if(user.getRole() == Role.ADMIN){
//                menuAdmin();
//            }else {
//                menuUser();
//            }
//        }
//    }

    public static void main(String[] args) {

    }

    public static void menuUser(){
        while (true){
            System.out.println("----".repeat(15));
            System.out.println("Mời bạn chọn chức năng : ");
            System.out.println("1. Danh sách User.");
            System.out.println("2. Tìm kiếm User theo ID.");
            System.out.println("3. Tìm kiếm User theo từ khóa có chứa trong email, username.");
            System.out.println("4. Hiển thị danh sách Department.");
            System.out.println("5. Tìm kiếm Department thep ID.");
            System.out.println("6. Tìm kiếm Department thep Name.");
            System.out.println("7. Thoát chương trình");
            System.out.println("-------------------------------------");

            int chose = ScannerUtils.inputNumber(1,7);
            switch (chose){
                case 1:
                    functionUser.getAllUser();
                    break;
                case 2:
                    functionUser.findById();
                    break;
                case 3:
                    functionUser.findByUserEmail();
                    break;
                case 4:
                    functionUser.getAllDepartment();
                    break;
                case 5:
                    functionUser.findByDepartmentId();
                    break;
                case 6:
                    functionUser.findByDepartmentName();
                    break;
                case 7:
                    return;
            }
        }
    }

    public static void menuAdmin(){
        while (true){
            System.out.println("----".repeat(15));
            System.out.println("Mời bạn chọn chức năng : ");
            System.out.println("1. Danh sách User.");
            System.out.println("2. Xóa User theo ID.");
            System.out.println("3. Thay đổi password của user.");
            System.out.println("4. Thêm mới 1 User, mặc định pass là 123456, role là user.");
            System.out.println("5. Hiện thị danh sách Department.");
            System.out.println("6. Xóa department theo id.");
            System.out.println("7. Thay đổi ten Department");
            System.out.println("8. Thoát chương trình");
            System.out.println("-------------------------------------");

            int chose = ScannerUtils.inputNumber(1,8);
            switch (chose){
                case 1:
                    functionAdmin.getAllUser();
                    break;
                case 2:
                    functionAdmin.deleteUser();
                    break;
                case 3:
                    functionAdmin.changePassword();
                    break;
                case 4:
                    functionAdmin.addUser();
                    break;
                case 5:
                    functionAdmin.getAllDepartment();
                    break;
                case 6:
                    functionAdmin.deleteDepartment();
                    break;
                case 7:
                    functionAdmin.updateDepartmentName();
                    break;
                case 8:
                    return;
            }
        }
    }
}
