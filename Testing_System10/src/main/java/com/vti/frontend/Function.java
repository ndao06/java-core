package com.vti.frontend;

import com.vti.backend.controller.AccountController;
import com.vti.entity.Account;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class Function {
    AccountController controller = new AccountController();

    public  void createAccount(){
        System.out.println("Mời bạn nhập UserName: ");
        String username = ScannerUtils.inputString();
        String email = ScannerUtils.inputEmail();
        String password = ScannerUtils.inputPassword();

        controller.createAccount(username,email,password);
    }

    public void updateAccount(){
        System.out.println("Mời bạn nhập ID của Account muốn sửa password: ");
        int id = ScannerUtils.inputNumber();
        System.out.println("Nhâp vào password cũ: ");
        String oldPassword = ScannerUtils.inputPassword();
        System.out.println("Nhập vào password mới: ");
        String newPassword = ScannerUtils.inputPassword();

        controller.updateAccount(id,oldPassword,newPassword);
    }

    public void deleteAccount(){
        System.out.println("Mời bạn nhập ID của Account muốn xóa: ");
        int number = ScannerUtils.inputNumber();

        controller.deleteAccount(number);

    }

    public void findByEmail(){
        System.out.println("Mời bạn nhập từ khóa muốn tìm kiếm");
        String email = ScannerUtils.inputString();
        List<Account> accountList = controller.findByEmail(email);

        String leftAlignFormat = "| %-3s| %-20s | %-25s | %-15s | %n";
        System.out.format("+----+--------------------+-------------------------+---------------+%n");
        System.out.format("| id |      fullName      |          email          |    password   |%n");
        System.out.format("+----+--------------------+-------------------------+---------------+%n");

        for(Account account: accountList){
            System.out.format(leftAlignFormat, account.getAccountId(), account.getFullName(), account.getEmail(), account.getPassword());
            System.out.format("+----+--------------------+-------------------------+---------------+%n");
        }

    }

    public void getAllAccount(){
        List<Account> accounts= controller.getAllAccount();
        String leftAlignFormat = "| %-3s| %-20s | %-25s | %-15s | %n";
        System.out.format("+----+--------------------+-------------------------+---------------+%n");
        System.out.format("| id |      fullName      |          email          |    password   |%n");
        System.out.format("+----+--------------------+-------------------------+---------------+%n");

        for(Account account: accounts){
            System.out.format(leftAlignFormat, account.getAccountId(), account.getFullName(), account.getEmail(), account.getPassword());
            System.out.format("+----+--------------------+-------------------------+---------------+%n");
        }
    }

    public  void login(){
        String email = ScannerUtils.inputEmail();
        String password = ScannerUtils.inputPassword();
        if(controller.login(email,password)){
            System.out.println("Đăng nhập thành công");
        }else {
            System.out.println("Đăng nhập thất bại");
        }
    }
}
