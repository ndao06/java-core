package com.vti.frontend;

import com.vti.backend.controller.AdminController;
import com.vti.entity.Department;
import com.vti.entity.User;
import com.vti.utils.ScannerUtils;

import java.util.List;

public class FunctionAdmin {
    AdminController adminController = new AdminController();

    public void getAllUser(){
        List<User> userList = adminController.getAllUser();
        String leftAlignFormat = "| %-3s| %-8s | %-15s | %-17s | %-15s | %-15s |%n";
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        System.out.format("| id |   role   |     username    |        email      | date_of_birth   | department_name |%n");
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
        for (User user: userList) {
            System.out.format(leftAlignFormat, user.getId(), user.getRole().name(), user.getUsername(), user.getEmail(),
                    user.getDate(), user.getDepartment().getDepartmentName());
        }
        System.out.format("+----+----------+-----------------+-------------------+-----------------+-----------------+%n");
    }

    public void deleteUser(){
        System.out.println("Mời bạn nhập vào id muốn xóa của user: ");
        int id = ScannerUtils.inputNumber();

        adminController.deleteUser(id);
    }

    public void changePassword(){
        System.out.println("Mời bạn nhập id của user muốn thay đổi pass: ");
        int id = ScannerUtils.inputNumber();
        System.out.println("Nhập password cũ: ");
        String oldPassword = ScannerUtils.inputPassword();
        System.out.println("Nhập password mới: ");
        String newPassword = ScannerUtils.inputPassword();

        adminController.changePassword(id,oldPassword,newPassword);
    }

    public void addUser(){
        System.out.println("Mời bạn nhập username: ");
        String username = ScannerUtils.inputString();
        System.out.println("Mời bạn nhập email: ");
        String email = ScannerUtils.inputEmail();
        System.out.println("Mời bạn nhập date: ");
        String date = ScannerUtils.inputString();
        System.out.println("Mời bạn nhập departmentId: ");
        int id = ScannerUtils.inputNumber();

        adminController.addUser(username,email,date,id);
    }

    public void getAllDepartment(){
        List<Department> departmentList = adminController.getAllDepartment();
        String leftAlignFormat = "| %-3s| %-15s |%n";
        System.out.format("+----+-----------------+%n");
        System.out.format("| id | department_name |%n");
        System.out.format("+----+-----------------+%n");
        for (Department department: departmentList) {
            System.out.format(leftAlignFormat,department.getId(),department.getDepartmentName());
        }
        System.out.format("+----+-----------------+%n");
    }

    public void deleteDepartment(){
        System.out.println("Mời bạn nhập vào id của department muốn xóa: ");
        int id = ScannerUtils.inputNumber();

        adminController.deleteDepartment(id);
    }

    public void updateDepartmentName(){
        System.out.println("Mời bạn nhập id của department muốn thay đổi tên: ");
        int id = ScannerUtils.inputNumber();
        System.out.println("Nhập department_name cũ: ");
        String oldName = ScannerUtils.inputString();
        System.out.println("Nhập department_name mới: ");
        String newName = ScannerUtils.inputString();
        adminController.updateDepartmentName(id,oldName,newName);
    }
}
