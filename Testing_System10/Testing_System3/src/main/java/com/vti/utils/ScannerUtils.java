package com.vti.utils;

import java.util.Scanner;

public class ScannerUtils {
    static Scanner scanner = new Scanner(System.in);

    public static String inputString(){
        return scanner.nextLine();
    }

    public static int inputNumber(){
        int chose = Integer.parseInt(scanner.nextLine());
        if(chose<=0){
            System.out.println("Mời nhập lại:");
            chose=Integer.parseInt(scanner.nextLine());
        }
        return chose;
    }
}
