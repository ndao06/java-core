package com.vti.backend;
import com.vti.entity.*;
public class Exercise5 {
    public static void main(String[] args) {


        Department[] departments = new Department[5];

        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Sale";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Marketing";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "Boss of director";

        Department department4 = new Department();
        department4.departmentId = 4;
        department4.departmentName = "Waiting room";

        Department department5 = new Department();
        department5.departmentId = 5;
        department5.departmentName = "Accounting";

        departments[0] = department1;
        departments[1] = department2;
        departments[2] = department3;
        departments[3] = department4;
        departments[4] = department5;

        //question1
        System.out.println(department1);

        //question2
        for (Department departmentt : departments){
            System.out.println(departmentt);
        }

        //question3
        System.out.println(department1.hashCode());

        //question4
        if(department1.departmentName.equals("Phòng A")){
            System.out.println("Phòng có tên là Phòng A");
        }else {
            System.out.println("Phòng không có tên là phòng A");
        }

        //question5
        if(department1.departmentName.equals(department2)){
            System.out.println("Bằng nhau");
        }else {
            System.out.println("Không bằng nhau");
        }

        //question6
        for (int i = 0; i < departments.length; i++) {
            for (int j = 0; j < departments.length; j++) {
                if(departments[i].departmentName.compareToIgnoreCase(departments[j].departmentName)<0){
                    Department temp= departments[i];
                    departments[j]=departments[i];
                    departments[i]=temp;
                }
            }
        }

        for (Department departmentv : departments) {
            System.out.println(departmentv);
        }

    }


    public static void question1 (Department department1){
        System.out.println(department1.toString());
        }

    public static void question2(Department[] departments) {
        for (Department department : departments) {
            System.out.println(department);
        }
    }

    }
