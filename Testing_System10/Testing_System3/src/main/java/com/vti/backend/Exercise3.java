package com.vti.backend;

public class Exercise3 {
    public static void question1(){
        System.out.println("-------question1--------");
        Integer salary = 5000;
        System.out.printf("%2.2f", (float) salary);
    }

    public static void question2(){
        System.out.println("-------question2--------");
        String s="12345";
        int i= Integer.parseInt(s);
        System.out.println(i);
    }

    public static void question3(){
        System.out.println("-------question2--------");
        Integer i = 1234567;
        int i1 = i.intValue();
        System.out.println(i1);
    }
}
