package com.vti.backend;

import com.vti.entity.*;
import com.vti.utils.ScannerUtils;

import java.util.Scanner;

public class Exercise4 {
    public static void question1() {
        System.out.println("------question1--------");
        String s;
        System.out.println("Mời nhập chuỗi: ");
        s = ScannerUtils.inputString();
        String[] string = s.split(" ");
        System.out.println("Số ký tự là: " + string.length);
    }

    public static void question2() {
        System.out.println("------question2--------");
        System.out.println("Mời nhập chuỗi thứ nhất: ");
        String string = ScannerUtils.inputString();
        System.out.println("Mời nhập chuỗi thứ hai: ");
        String string1 = ScannerUtils.inputString();
        System.out.println("Kết quả sau khi xâu chuỗi là: " + string + " " + string1);
    }

    public static void question3() {
        System.out.println("------question3--------");
        System.out.println("Mời nhập tên: ");
        String string = ScannerUtils.inputString();
        String s = string.substring(0, 1).toUpperCase();
        String s1 = string.substring(1);
        System.out.println("Tên là: " + s.concat(s1));
    }

    public static void question4() {
        System.out.println("------question4--------");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Mời nhập tên: ");
        String string = scanner.nextLine();
        string=string.toUpperCase();
        for (int i = 0; i < string.length(); i++) {
            System.out.println("Ký tự thứ: "+i+ " là: "+string.charAt(i));
        }
    }

    public static void question5(){
        System.out.println("------question5--------");
        System.out.println("Mời nhập họ: ");
        String string = ScannerUtils.inputString();
        System.out.println("Mời nhập tên: ");
        String string1 = ScannerUtils.inputString();
        System.out.println("Họ và tên là:"+string.concat(string1));
    }

    public static void question6(){
        System.out.println("------question6--------");
        System.out.println("Mời nhập họ và tên: ");
        String string= ScannerUtils.inputString();
        string=string.trim();
        String[] s = string.split(" ");
        String lastName ="",firstName="",middleName="";
        lastName = s[0];
        firstName = s[s.length - 1];
        for (int i = 1; i <= s.length - 2; i++) {
            middleName += s[i] + " ";
        }
        System.out.println("Họ là: " + lastName);
        System.out.println("Tên đệm là: " + middleName);
        System.out.println("Tên là: " + firstName);
    }

    public static void question7(){
        System.out.println("------question7--------");
        Scanner scanner = new Scanner(System.in);
    }

    public static void question8(){
        System.out.println("------question8--------");
        String[] string = {"Java","C","Bài tập Java"};
        for(String string1:string){
            if(string1.contains("Java")){
                System.out.println(string1);
            }
        }
    }

    public static void question9(){
        System.out.println("------question9--------");
        String[] string = {"Java","C","Bài tập Java"};
        for(String string1:string){
            if(string1.equals("Java")){
                System.out.println(string1);
            }
        }
    }

    public static void question10(){
        System.out.println("------question10--------");
        System.out.println("Nhập chuỗi 1: ");
        String string = ScannerUtils.inputString();
        System.out.println("Nhập chuỗi 2: ");
        String string1 = ScannerUtils.inputString();
        for (int i = 0; i < string.length(); i++) {
            for (int j = string.length(); j > -1; j++) {
                if(string.charAt(i)==string.charAt(j)){
                    System.out.println("Đây là chuỗi đảo ngược");
                }else {
                    System.out.println("Đây không  phải chuỗi đảo ngược");
                }
            }
        }
    }

    public static void question11(){
        System.out.println("------question11--------");
        System.out.println("Mời bạn nhập chuỗi: ");
        String string = ScannerUtils.inputString();
        int count=0;
        for (int i = 0; i < string.length(); i++) {
            if(string.charAt(i)=='a'){
                count++;
            }
        }
        System.out.println(count);
    }

    public static void question12(){
        System.out.println("------question12--------");
        System.out.println("Mời nhập chuỗi: ");
        String string= ScannerUtils.inputString();
        String string1="";
        for (int i = string.length(); i >-1; i++) {
            string1= string1+ string.charAt(i);
        }
        System.out.println("Chuỗi đảo ngược là: "+string1);
    }

    public static void question13(){
        System.out.println("------question13--------");
        System.out.println("Mời nhập chuỗi: ");
        String string= ScannerUtils.inputString();
        for (int i = 0; i < string.length(); i++) {
            if(string.charAt(i)==48 ||string.charAt(i)==57){
                System.out.println("false");
            }else {
                System.out.println("true");
            }
        }
    }

    public static void question14(){
        System.out.println("------question14--------");
        System.out.println("Mời nhập chuỗi: ");
        String string= ScannerUtils.inputString();
        System.out.println("Nhập ký tự muốn chuyến: ");
        char a=ScannerUtils.inputString().toCharArray()[0];
        System.out.println("Nhập ký tự sẽ chuyến: ");
        char b=ScannerUtils.inputString().toCharArray()[0];
        for (int i = 0; i < string.length(); i++) {
            if(string.charAt(i)==a){
                string=string.replace(a,b);
            }
        }
        System.out.println("Chuỗi sau khi chuyển là: "+string);
    }
}