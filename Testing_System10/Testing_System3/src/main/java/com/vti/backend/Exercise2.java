package com.vti.backend;
import com.vti.entity.*;

import java.time.LocalDate;

public class Exercise2 {
    public static void question1(){
        System.out.println("-----question1------");
        Account[] arrAccount = new Account[5];
        for (int i = 0; i < arrAccount.length; i++) {
            Account account = new Account();
            account.email="Email: "+i;
            account.userName="UserName: "+i;
            account.fullName="FullName: "+i;
            account.createDate= LocalDate.now();
            arrAccount[i] = account;
            System.out.println("Thông tin account: "+i+
                    "Email: "+arrAccount[i].email+
                    "UserName: "+arrAccount[i].userName+
                    "FullName: "+arrAccount[i].fullName+
                    "CreateDate: "+arrAccount[i].createDate);
        }
    }
}
