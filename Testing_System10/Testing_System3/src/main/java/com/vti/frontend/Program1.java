package com.vti.frontend;
import com.vti.backend.*;
import com.vti.utils.ScannerUtils;

public class Program1 {
    public static void main(String[] args) {
        while (true){
            System.out.println("---------Exercise1--------");
            System.out.println("1: question1");
            System.out.println("2: question2");
            System.out.println("3: question3");
            System.out.println("Chọn question muốn xem: ");
            int chose = ScannerUtils.inputNumber();

            switch (chose){
                case 1:
                    Exercise1.question1();
                    break;
                case 2:
                    Exercise1.question2();
                    break;
                case 3:
                    Exercise1.question4();
                    break;
                default:
                    System.out.println("Mời nhập lại question muốn xem: ");
                    chose= ScannerUtils.inputNumber();
            }
        }
    }
}
