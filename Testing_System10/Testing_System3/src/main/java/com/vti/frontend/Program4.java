package com.vti.frontend;

import com.vti.backend.Exercise4;
import com.vti.utils.ScannerUtils;

public class Program4 {
    public static void main(String[] args) {
        while (true){
            System.out.println("---------Exercise4---------");
            System.out.println("Question từ 1 ->14 ");
            System.out.println("Chọn question muốn xem: ");
            int chose= ScannerUtils.inputNumber();

            switch (chose){
                case 1:
                    Exercise4.question1();
                    break;
                case 2:
                    Exercise4.question2();
                    break;
                case 3:
                    Exercise4.question3();
                    break;
                case 4:
                    Exercise4.question4();
                    break;
                case 5:
                    Exercise4.question5();
                    break;
                case 6:
                    Exercise4.question6();
                    break;
                case 7:
                    Exercise4.question7();
                    break;
                case 8:
                    Exercise4.question8();
                    break;
                case 9:
                    Exercise4.question9();
                    break;
                case 10:
                    Exercise4.question10();
                    break;
                case 11:
                    Exercise4.question11();
                    break;
                case 12:
                    Exercise4.question12();
                    break;
                case 13:
                    Exercise4.question13();
                    break;
                case 14:
                    Exercise4.question14();
                    break;
                default:
                    System.out.println("Không có question bạn vừa nhập");
                    return;
            }
        }
    }
}
